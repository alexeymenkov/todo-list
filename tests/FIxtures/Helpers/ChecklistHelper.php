<?php declare(strict_types=1);


namespace Tests\DemoCode\TodoList\FIxtures\Helpers;


use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Domain\Todo;
use DemoCode\TodoList\Application\Service\ChecklistPersister;
use Symfony\Bundle\FrameworkBundle\Client;

class ChecklistHelper
{
    public static function loadFixtures(Client $client, string $checklistId): Checklist
    {
        /** @var ChecklistPersister $persister */
        $persister = $client->getContainer()->get(ChecklistPersister::class);
        $checklist = new Checklist(
            new ChecklistId($checklistId),
            "title",
            [Todo::createNew("todo title", false, 0)],
            new \DateTimeImmutable(),
            new \DateTimeImmutable()
        );

        $persister->saveChecklist($checklist);

        return $checklist;
    }
}