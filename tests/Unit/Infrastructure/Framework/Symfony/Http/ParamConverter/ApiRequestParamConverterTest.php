<?php

namespace Tests\DemoCode\TodoList\Unit\Infrastructure\Framework\Symfony\Http\ParamConverter;

use DemoCode\TodoList\Entry\Api\ApiRequest;
use DemoCode\TodoList\Infrastructure\Framework\Symfony\Http\ParamConverter\ApiRequestParamConverter;
use PHPUnit\Framework\TestCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class ApiRequestParamConverterTest extends TestCase
{
    /**
     * @var ApiRequestParamConverter
     */
    private $converter;

    protected function setUp(): void
    {
        $this->converter = new ApiRequestParamConverter();
    }

    public function testApply(): void
    {
        $request = Request::create("/", "GET", [], [], [], [], json_encode(["data" => "foobar"]));
        $configuration = new ParamConverter(["class" => TestApiRequest::class, "name" => "request"]);

        $result = $this->converter->apply($request, $configuration);

        expect($result)->true();
        expect($request->attributes->has("request"))->true();
        /** @var TestApiRequest $apiRequest */
        $apiRequest = $request->attributes->get("request");
        expect($apiRequest)->isInstanceOf(TestApiRequest::class);
        expect($apiRequest->getData())->equals("foobar");
    }

    /**
     * @test
     */
    public function appliesOnlyForApiRequest(): void
    {
        $result = $this->converter->apply(
            Request::createFromGlobals(),
            new ParamConverter(["class" => \DateTime::class])
        );

        expect($result)->false();
    }

    /**
     * @test
     * @dataProvider supportsExamples
     * @param ParamConverter $configuration
     * @param bool $expected
     */
    public function testSupports(ParamConverter $configuration, bool $expected): void
    {
        expect($this->converter->supports($configuration))->same($expected);
    }

    /**
     * @return array|array[]
     */
    public function supportsExamples(): array
    {
        return [
            [new ParamConverter(["class" => \DateTime::class]), false],
            [new ParamConverter(["class" => null]), false],
            [new ParamConverter(["class" => TestApiRequest::class]), true],
        ];
    }
}

class TestApiRequest implements ApiRequest
{
    /**
     * @var string|null
     */
    private $data;

    public function mergeFromJsonString(string $json): void
    {
        $decoded = json_decode($json, true);
        $this->data = $decoded["data"] ?? null;
    }

    /**
     * @return string|null
     */
    public function getData(): ?string
    {
        return $this->data;
    }
}