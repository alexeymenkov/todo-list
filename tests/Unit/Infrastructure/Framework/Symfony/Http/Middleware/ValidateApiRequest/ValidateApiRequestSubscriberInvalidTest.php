<?php

namespace Tests\DemoCode\TodoList\Unit\Infrastructure\Framework\Symfony\Http\Middleware\ValidateApiRequest;

use DemoCode\TodoList\Entry\Api;
use DemoCode\TodoList\Infrastructure\Framework\Symfony\Http\Middleware\ValidateApiRequest\RequestValidationException;
use DemoCode\TodoList\Infrastructure\Framework\Symfony\Http\Middleware\ValidateApiRequest\ValidateApiRequestSubscriber;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerArgumentsEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidateApiRequestSubscriberTest extends TestCase
{
    /**
     * @var ObjectProphecy|ValidatorInterface
     */
    private $validator;
    /**
     * @var ValidateApiRequestSubscriber
     */
    private $subscriber;

    protected function setUp(): void
    {
        $this->validator = $this->prophesize(ValidatorInterface::class);
        $this->subscriber = new ValidateApiRequestSubscriber(
            $this->validator->reveal()
        );
    }

    /**
     * @test
     */
    public function getSubscribedEvents(): void
    {
        $events = $this->subscriber::getSubscribedEvents();
        expect($events)->equals(
            [
                KernelEvents::CONTROLLER_ARGUMENTS => "onControllerArguments",
                KernelEvents::EXCEPTION => "onException",
            ]
        );
    }

    /**
     * @test
     */
    public function validatesApiRequestArguments(): void
    {
        $invalidTestApiRequest = new InvalidTestApiRequest();
        $validApiRequest = new ValidTestApiRequest();
        $event = $this->createControllerEvent(["test", $validApiRequest, $invalidTestApiRequest]);

        $this->validator->validate($invalidTestApiRequest)
            ->shouldBeCalled()
            ->willReturn(new ConstraintViolationList([
                new ConstraintViolation("invalid value", "", [], "", "path", "")]
            ));

        $this->validator->validate($validApiRequest)
            ->shouldBeCalled()
            ->willReturn(new ConstraintViolationList([]));

        try {
            $this->subscriber->onControllerArguments($event);
        } catch (\Throwable $exception) {
            /** @var RequestValidationException $exception */
            expect($exception)->isInstanceOf(RequestValidationException::class);
            $constraintViolationList = $exception->getConstraintViolationList();
            expect($constraintViolationList)->count(1);
            expect($constraintViolationList)->containsOnlyInstancesOf(Api\ConstraintViolation::class);
            expect($constraintViolationList)->hasKey(0);
            expect($constraintViolationList[0]->getMessage())->equals("invalid value");
            expect($constraintViolationList[0]->getPropertyPath())->equals("path");
        }
    }

    /**
     * @test
     */
    public function handlesRequestValidationException(): void
    {
        $exception = new RequestValidationException([new Api\ConstraintViolation("path", "message")]);
        $event = $this->createExceptionEvent($exception);

        $this->subscriber->onException($event);

        expect($event->hasResponse())->true();
        expect($event->getResponse())->isInstanceOf(JsonResponse::class);
        expect($event->getResponse()->getStatusCode())->equals(Response::HTTP_BAD_REQUEST);
        $content = $event->getResponse()->getContent();
        $data = json_decode($content, true);
        expect($data)->equals([
            "data" => null,
            "error" => [
                "message" => "Validation error",
                "code" => "validation_error",
                "violations" => [
                    ["propertyPath" => "path", "message" => "message"]
                ]
            ]
        ]);
    }

    /**
     * @test
     */
    public function supportsOnlyRequestValidationException(): void
    {
        $event = $this->createExceptionEvent(new \Exception());
        $this->subscriber->onException($event);

        expect($event->hasResponse())->false();
    }

    /**
     * @param array|mixed[] $erguments
     * @return FilterControllerArgumentsEvent
     */
    private function createControllerEvent(array $erguments): FilterControllerArgumentsEvent
    {
        return new FilterControllerArgumentsEvent(
            new TestKernel(),
            [$this, "getSubscribedEvents"],
            $erguments,
            Request::createFromGlobals(),
            HttpKernelInterface::MASTER_REQUEST
        );
    }

    /**
     * @param \Exception $excption
     * @return GetResponseForExceptionEvent
     */
    private function createExceptionEvent(\Exception $excption): GetResponseForExceptionEvent
    {
        return new GetResponseForExceptionEvent(
            new TestKernel(),
            Request::createFromGlobals(),
            HttpKernelInterface::MASTER_REQUEST,
            $excption
        );
    }
}

class TestKernel implements HttpKernelInterface
{
    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        return new Response();
    }
}

class InvalidTestApiRequest implements Api\ApiRequest
{
    public function mergeFromJsonString(string $json): void
    {

    }
}

class ValidTestApiRequest implements Api\ApiRequest
{
    public function mergeFromJsonString(string $json): void
    {
        
    }
}

