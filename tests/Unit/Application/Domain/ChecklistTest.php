<?php

namespace Tests\DemoCode\TodoList\Unit\Application\Domain;

use DemoCode\TodoList\Application\Domain;
use PHPUnit\Framework\TestCase;

class ChecklistTest extends TestCase
{
    /**
     * @test
     */
    public function doesNotRemoveMissingTodo(): void
    {
        $checklist = Domain\Checklist::createNew("title");
        $checklist->addTodo(Domain\Todo::createNew("todo", false, 0));

        $result = $checklist->removeTodo((new Domain\TodoId("12345")));
        expect($result)->same($checklist);
    }

    /**
     * @throws \Assert\AssertionFailedException
     * @test
     */
    public function throwsWhenGetMissingTodo(): void
    {
        $this->expectException(\Assert\AssertionFailedException::class);
        $checklist = Domain\Checklist::createNew("title");

        $checklist->getTodo(Domain\TodoId::new());
    }
}
