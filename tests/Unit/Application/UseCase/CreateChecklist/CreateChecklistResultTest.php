<?php

namespace Tests\DemoCode\TodoList\Unit\Application\UseCase\CreateChecklist;

use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\CreateChecklist\CreateChecklistResult;
use PHPUnit\Framework\TestCase;

class CreateChecklistResultTest extends TestCase
{
    /**
     * @test
     */
    public function throwsOnGetChecklistWhenResultHasError(): void
    {
        $this->expectException(\LogicException::class);

        $result = CreateChecklistResult::createWithError(ChecklistOperationError::ChecklistNotFound());
        $result->getChecklist();
    }
}
