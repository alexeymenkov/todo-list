<?php

namespace Tests\DemoCode\TodoList\Unit\Application\UseCase\CreateChecklist;

use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Service\ChecklistPersister;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\CreateChecklist\CreateChecklistRequest;
use DemoCode\TodoList\Application\UseCase\CreateChecklist\CreateChecklistResult;
use DemoCode\TodoList\Application\UseCase\CreateChecklist\Interactor;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;

class InteractorTest extends TestCase
{
    /**
     * @var ObjectProphecy|ChecklistPersister
     */
    private $persister;
    /**
     * @var Interactor
     */
    private $interactor;

    protected function setUp(): void
    {
        $this->persister = $this->prophesize(ChecklistPersister::class);
        $this->interactor = new Interactor($this->persister->reveal());
    }

    /**
     * @test
     */
    public function addChecklist(): void
    {
        /** @var Checklist $checklist */
        $checklist = null;
        $this->persister->saveChecklist(Argument::type(Checklist::class))
            ->shouldBeCalled()
            ->will(function (array $args) use(&$checklist) {
                $checklist = $args[0];
            });

        $result = $this->interactor->createChecklist(new CreateChecklistRequest("checklist title"));

        expect($result)->isInstanceOf(CreateChecklistResult::class);
        expect($result->isSuccessful())->true();
        expect($result->getError())->equals(ChecklistOperationError::None());
        expect($result->getChecklist())->isInstanceOf(Checklist::class);
        expect($result->getChecklist())->same($checklist);
        expect($result->getChecklist()->getTitle())->equals("checklist title");
        expect($result->getChecklist()->getTodoList())->isEmpty();
        expect($result->getChecklist()->getId()->toString())->notEmpty();
        expect($result->getChecklist()->getCreatedAt())->isInstanceOf(\DateTimeImmutable::class);
        expect($result->getChecklist()->getUpdatedAt())->isInstanceOf(\DateTimeImmutable::class);
    }
}
