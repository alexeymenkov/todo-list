<?php

namespace Tests\DemoCode\TodoList\Unit\Application\UseCase\DeleteChecklist;

use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\DeleteChecklist\ChecklistDeleter;
use DemoCode\TodoList\Application\UseCase\DeleteChecklist\DeleteChecklistResult;
use DemoCode\TodoList\Application\UseCase\DeleteChecklist\Interactor;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class InteractorTest extends TestCase
{
    /**
     * @var ObjectProphecy|ChecklistDeleter
     */
    private $deleter;
    /**
     * @var Interactor
     */
    private $interactor;

    protected function setUp(): void
    {
        $this->deleter = $this->prophesize(ChecklistDeleter::class);
        $this->interactor = new Interactor($this->deleter->reveal());
    }

    /**
     * @test
     */
    public function deleteChecklist(): void
    {
        $checklistId = ChecklistId::new();
        $this->deleter->deleteChecklist($checklistId)
            ->shouldBeCalled();

        $result = $this->interactor->deleteChecklist($checklistId);

        expect($result)->isInstanceOf(DeleteChecklistResult::class);
        expect($result->isSuccessful())->true();
        expect($result->getError())->equals(ChecklistOperationError::None());
    }
}
