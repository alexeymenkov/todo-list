<?php

namespace Tests\DemoCode\TodoList\Unit\Application\UseCase\UpdateTodo;

use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Domain\Todo;
use DemoCode\TodoList\Application\Domain\TodoId;
use DemoCode\TodoList\Application\Service\ChecklistPersister;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\UpdateTodo\Interactor;
use DemoCode\TodoList\Application\UseCase\UpdateTodo\UpdateTodoRequest;
use DemoCode\TodoList\Application\UseCase\UpdateTodo\UpdateTodoResult;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;

class InteractorTest extends TestCase
{
    /**
     * @var ObjectProphecy|ChecklistRepository
     */
    private $repository;
    /**
     * @var ObjectProphecy|ChecklistPersister
     */
    private $persister;
    /**
     * @var Interactor
     */
    private $interactor;

    protected function setUp(): void
    {
        $this->repository = $this->prophesize(ChecklistRepository::class);
        $this->persister = $this->prophesize(ChecklistPersister::class);
        $this->interactor = new Interactor(
            $this->repository->reveal(),
            $this->persister->reveal()
        );
    }

    /**
     * @test
     */
    public function updateTodo(): void
    {
        $checklistId = ChecklistId::new();
        $todo = Todo::createNew("title", false, 0);

        $checklist = Checklist::createNew("checklist title");
        $checklist->addTodo($todo);

        $this->repository->findChecklist($checklistId)
            ->shouldBeCalled()
            ->willReturn($checklist);

        /** @var Checklist $updatedChecklist */
        $updatedChecklist = null;
        $this->persister->saveChecklist(Argument::type(Checklist::class))
            ->shouldBeCalled()
            ->will(function (array $args) use (&$updatedChecklist) {
                 $updatedChecklist = $args[0];
            });

        $updateTodoRequest = new UpdateTodoRequest($checklistId, $todo->getId(), "new title", true, 1);

        $result = $this->interactor->updateTodo($updateTodoRequest);

        expect($result)->isInstanceOf(UpdateTodoResult::class);
        expect($result->isSuccessful())->true();
        expect($result->getError())->equals(ChecklistOperationError::None());
        expect($result->getTodo())->same($updatedChecklist->getTodo($todo->getId()));
        expect($result->getTodo()->getTitle())->equals("new title");
        expect($result->getTodo()->isCompleted())->true();
        expect($result->getTodo()->getPosition())->same(1);

        expect($updatedChecklist->getTodoList())->count(1);
    }

    /**
     * @test
     */
    public function returnsErrorWhenChecklistNotFound(): void
    {
        $checklistId = ChecklistId::new();
        $this->repository->findChecklist($checklistId)
            ->shouldBeCalled()
            ->wilLReturn(null);

        $updateTodoRequest = new UpdateTodoRequest($checklistId, TodoId::new(), "new title", true, 1);
        $result = $this->interactor->updateTodo($updateTodoRequest);

        expect($result)->isInstanceOf(UpdateTodoResult::class);
        expect($result->isSuccessful())->false();
        expect($result->getError())->equals(ChecklistOperationError::ChecklistNotFound());

        $this->persister->saveChecklist(Argument::type(Checklist::class))
            ->shouldNotHaveBeenCalled();
    }

    /**
     * @test
     */
    public function returnsErrorWhenTodoNotFound(): void
    {
        $checklist = Checklist::createNew("title");
        $this->repository->findChecklist($checklist->getId())
            ->shouldBeCalled()
            ->wilLReturn($checklist);

        $updateTodoRequest = new UpdateTodoRequest($checklist->getId(), TodoId::new(), "new title", true, 1);
        $result = $this->interactor->updateTodo($updateTodoRequest);

        expect($result)->isInstanceOf(UpdateTodoResult::class);
        expect($result->isSuccessful())->false();
        expect($result->getError())->equals(ChecklistOperationError::TodoNotFound());

        $this->persister->saveChecklist(Argument::type(Checklist::class))
            ->shouldNotHaveBeenCalled();
    }
}
