<?php

namespace Tests\DemoCode\TodoList\Unit\Application\UseCase\UpdateTodo;

use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\UpdateTodo\UpdateTodoResult;
use PHPUnit\Framework\TestCase;

class UpdateTodoResultTest extends TestCase
{
    /**
     * @test
     */
    public function throwsOnGetTodoWhenResultHasError(): void
    {
        $this->expectException(\LogicException::class);

        $result = UpdateTodoResult::createWithError(ChecklistOperationError::ChecklistNotFound());
        $result->getTodo();
    }

}
