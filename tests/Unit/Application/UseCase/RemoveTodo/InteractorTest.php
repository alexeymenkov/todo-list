<?php

namespace Tests\DemoCode\TodoList\Unit\Application\UseCase\RemoveTodo;

use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Domain\Todo;
use DemoCode\TodoList\Application\Domain\TodoId;
use DemoCode\TodoList\Application\Service\ChecklistPersister;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\RemoveTodo\Interactor;
use DemoCode\TodoList\Application\UseCase\RemoveTodo\RemoveTodoResult;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;

class InteractorTest extends TestCase
{
    /**
     * @var ObjectProphecy|ChecklistRepository
     */
    private $repository;
    /**
     * @var ObjectProphecy|ChecklistPersister
     */
    private $persister;
    /**
     * @var Interactor
     */
    private $interactor;

    protected function setUp(): void
    {
        $this->repository = $this->prophesize(ChecklistRepository::class);
        $this->persister = $this->prophesize(ChecklistPersister::class);
        $this->interactor = new Interactor(
            $this->repository->reveal(),
            $this->persister->reveal()
        );
    }

    /**
     * @test
     */
    public function removeTodo(): void
    {
        $checklistId = ChecklistId::new();

        $todo1 = Todo::createNew("todoTitle1", false, 0);
        $todo2 = Todo::createNew("todoTitle1", true, 1);
        $checklist = new Checklist(
            $checklistId,
            "title",
            [
                $todo1,
                $todo2,
            ],
            new \DateTimeImmutable(),
            new \DateTimeImmutable()
        );

        $this->repository->findChecklist($checklistId)
            ->shouldBeCalled()
            ->willReturn($checklist);

        /** @var Checklist $updatedChecklist */
        $updatedChecklist = null;
        $this->persister->saveChecklist(Argument::type(Checklist::class))
            ->shouldBeCalled()
            ->will(function (array $args) use (&$updatedChecklist) {
                $updatedChecklist = $args[0];
            });

        $result = $this->interactor->removeTodo($checklistId, $todo1->getId());

        expect($result)->isInstanceOf(RemoveTodoResult::class);
        expect($result->isSuccessful())->true();
        expect($result->getError())->equals(ChecklistOperationError::None());

        expect($updatedChecklist->getTodoList())->equals([$todo2]);
    }

    /**
     * @test
     */
    public function returnsErrorWhenChecklistNotFound(): void
    {
        $checklistId = ChecklistId::new();
        $this->repository->findChecklist($checklistId)
            ->shouldBeCalled()
            ->wilLReturn(null);

        $result = $this->interactor->removeTodo($checklistId, TodoId::new());

        expect($result)->isInstanceOf(RemoveTodoResult::class);
        expect($result->isSuccessful())->false();
        expect($result->getError())->equals(ChecklistOperationError::ChecklistNotFound());

        $this->persister->saveChecklist(Argument::type(Checklist::class))
            ->shouldNotHaveBeenCalled();
    }

    /**
     * @test
     */
    public function returnsErrorWhenTodoNotFound(): void
    {
        $checklist = Checklist::createNew("title");
        $this->repository->findChecklist($checklist->getId())
            ->shouldBeCalled()
            ->wilLReturn($checklist);

        $result = $this->interactor->removeTodo($checklist->getId(), TodoId::new());

        expect($result)->isInstanceOf(RemoveTodoResult::class);
        expect($result->isSuccessful())->false();
        expect($result->getError())->equals(ChecklistOperationError::TodoNotFound());

        $this->persister->saveChecklist(Argument::type(Checklist::class))
            ->shouldNotHaveBeenCalled();
    }

}
