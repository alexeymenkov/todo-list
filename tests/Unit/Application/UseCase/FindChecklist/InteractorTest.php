<?php

namespace Tests\DemoCode\TodoList\Unit\Application\UseCase\FindChecklist;

use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\FindChecklist\FindChecklistResult;
use DemoCode\TodoList\Application\UseCase\FindChecklist\Interactor;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class InteractorTest extends TestCase
{
    /**
     * @var ObjectProphecy|ChecklistRepository
     */
    private $repository;
    /**
     * @var Interactor
     */
    private $interactor;

    protected function setUp(): void
    {
        $this->repository = $this->prophesize(ChecklistRepository::class);
        $this->interactor = new Interactor($this->repository->reveal());
    }

    /**
     * @test
     */
    public function findChecklist(): void
    {
        $checklistId = ChecklistId::new();

        $checklist = Checklist::createNew("title");
        $this->repository->findChecklist($checklistId)
            ->shouldBeCalled()
            ->willReturn($checklist);

        $result = $this->interactor->findChecklist($checklistId);

        expect($result)->isInstanceOf(FindChecklistResult::class);
        expect($result->isSuccessful())->true();
        expect($result->getError())->equals(ChecklistOperationError::None());
        expect($result->getChecklist())->same($checklist);
    }

    /**
     * @test
     */
    public function returnsErrorWhenChecklistNotFound(): void
    {
        $checklistId = ChecklistId::new();
        $this->repository->findChecklist($checklistId)
            ->shouldBeCalled()
            ->wilLReturn(null);

        $result = $this->interactor->findChecklist($checklistId);

        expect($result)->isInstanceOf(FindChecklistResult::class);
        expect($result->isSuccessful())->false();
        expect($result->getError())->equals(ChecklistOperationError::ChecklistNotFound());
    }
}
