<?php

namespace Tests\DemoCode\TodoList\Unit\Application\UseCase\AddTodo;

use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\AddTodo\AddTodoResult;
use PHPUnit\Framework\TestCase;

class AddTodoResultTest extends TestCase
{
    /**
     * @test
     */
    public function throwsOnGetTodoWhenResultHasError(): void
    {
        $this->expectException(\LogicException::class);

        $result = AddTodoResult::createWithError(ChecklistOperationError::ChecklistNotFound());
        $result->getTodo();
    }
}
