<?php

namespace Tests\DemoCode\TodoList\Unit\Application\UseCase\AddTodo;

use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Service\ChecklistPersister;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\AddTodo\AddTodoRequest;
use DemoCode\TodoList\Application\UseCase\AddTodo\AddTodoResult;
use DemoCode\TodoList\Application\UseCase\AddTodo\Interactor;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;

class InteractorTest extends TestCase
{
    /**
     * @var ObjectProphecy|ChecklistRepository
     */
    private $repository;
    /**
     * @var ObjectProphecy|ChecklistPersister
     */
    private $persister;
    /**
     * @var Interactor
     */
    private $interactor;

    protected function setUp(): void
    {
        $this->repository = $this->prophesize(ChecklistRepository::class);
        $this->persister = $this->prophesize(ChecklistPersister::class);
        $this->interactor = new Interactor($this->repository->reveal(), $this->persister->reveal());
    }

    /**
     * @test
     */
    public function addTodo(): void
    {
        $checklist = Checklist::createNew("title");

        $this->repository->findChecklist($checklist->getId())
            ->shouldBeCalled()
            ->willReturn($checklist);

        /** @var Checklist $updatedChecklist */
        $updatedChecklist = null;
        $this->persister->saveChecklist(Argument::type(Checklist::class))
            ->shouldBeCalled()
            ->will(function (array $args) use (&$updatedChecklist) {
                $updatedChecklist = $args[0];
            });

        $result = $this->interactor->addTodo(new AddTodoRequest($checklist->getId(), "todoTitle", false, 2));

        expect($result)->isInstanceOf(AddTodoResult::class);
        expect($result->isSuccessful())->true();
        expect($result->getError())->equals(ChecklistOperationError::None());

        $todo = $result->getTodo();
        expect($todo)->same($updatedChecklist->getTodoList()[0]);
        expect($todo->getId()->toString())->notEmpty();
        expect($todo->getTitle())->equals("todoTitle");
        expect($todo->getPosition())->same(2);
        expect($todo->isCompleted())->same(false);
        expect($todo->getCreatedAt())->isInstanceOf(\DateTimeImmutable::class);
        expect($todo->getUpdatedAt())->isInstanceOf(\DateTimeImmutable::class);
    }

    /**
     * @test
     */
    public function returnsErrorWhenChecklistNotFound(): void
    {
        $checklistId = ChecklistId::new();

        $this->repository->findChecklist($checklistId)
            ->shouldBeCalled()
            ->willReturn(null);

        $result = $this->interactor->addTodo(new AddTodoRequest($checklistId, "title", true, 3));

        expect($result)->isInstanceOf(AddTodoResult::class);
        expect($result->isSuccessful())->false();
        expect($result->getError())->equals(ChecklistOperationError::ChecklistNotFound());

        $this->persister->saveChecklist(Argument::type(Checklist::class))->shouldNotHaveBeenCalled();
    }
}
