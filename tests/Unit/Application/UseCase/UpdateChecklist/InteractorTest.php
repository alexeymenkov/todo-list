<?php

namespace Tests\DemoCode\TodoList\Unit\Application\UseCase\UpdateChecklist;

use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Service\ChecklistPersister;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\UpdateChecklist\Interactor;
use DemoCode\TodoList\Application\UseCase\UpdateChecklist\UpdateChecklistRequest;
use DemoCode\TodoList\Application\UseCase\UpdateChecklist\UpdateChecklistResult;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;

class InteractorTest extends TestCase
{
    /**
     * @var ObjectProphecy|ChecklistRepository
     */
    private $repository;
    /**
     * @var ObjectProphecy|ChecklistPersister
     */
    private $persister;
    /**
     * @var Interactor
     */
    private $interactor;

    protected function setUp(): void
    {
        $this->repository = $this->prophesize(ChecklistRepository::class);
        $this->persister = $this->prophesize(ChecklistPersister::class);
        $this->interactor = new Interactor(
            $this->repository->reveal(),
            $this->persister->reveal()
        );
    }

    /**
     * @test
     */
    public function updateChecklist(): void
    {
        $checklistId = ChecklistId::new();

        $checklist = Checklist::createNew("title");
        $this->repository->findChecklist($checklistId)
            ->shouldBeCalled()
            ->willReturn($checklist);

        $updatedChecklist = null;
        $this->persister->saveChecklist(Argument::type(Checklist::class))
            ->shouldBeCalled()
            ->will(function (array $args) use (&$updatedChecklist) {
                 $updatedChecklist = $args[0];
            });

        $result = $this->interactor->updateChecklist(new UpdateChecklistRequest($checklistId, "new title"));

        expect($result)->isInstanceOf(UpdateChecklistResult::class);
        expect($result->isSuccessful())->true();
        expect($result->getError())->equals(ChecklistOperationError::None());
        expect($result->getChecklist())->same($updatedChecklist);
        expect($result->getChecklist()->getTitle())->equals("new title");
    }

    /**
     * @test
     */
    public function returnsErrorWhenChecklistNotFound(): void
    {
        $checklistId = ChecklistId::new();
        $this->repository->findChecklist($checklistId)
            ->shouldBeCalled()
            ->wilLReturn(null);

        $result = $this->interactor->updateChecklist(new UpdateChecklistRequest($checklistId, "new title"));

        expect($result)->isInstanceOf(UpdateChecklistResult::class);
        expect($result->isSuccessful())->false();
        expect($result->getError())->equals(ChecklistOperationError::ChecklistNotFound());

        $this->persister->saveChecklist(Argument::type(Checklist::class))->shouldNotHaveBeenCalled();
    }
}
