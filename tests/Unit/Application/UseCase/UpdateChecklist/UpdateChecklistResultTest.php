<?php

namespace Tests\DemoCode\TodoList\Unit\Application\UseCase\UpdateChecklist;

use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\UpdateChecklist\UpdateChecklistResult;
use PHPUnit\Framework\TestCase;

class UpdateChecklistResultTest extends TestCase
{
    /**
     * @test
     */
    public function throwsOnGetChecklistWhenResultHasError(): void
    {
        $this->expectException(\LogicException::class);

        $result = UpdateChecklistResult::createWithError(ChecklistOperationError::ChecklistNotFound());
        $result->getChecklist();
    }
}
