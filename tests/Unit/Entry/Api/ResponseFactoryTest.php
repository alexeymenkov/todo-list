<?php

namespace Tests\DemoCode\TodoList\Unit\Entry\Api;

use Assert\AssertionFailedException;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Entry\Api\ResponseFactory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ResponseFactoryTest extends TestCase
{
    /**
     * @var ResponseFactory
     */
    private $factory;

    protected function setUp(): void
    {
        $this->factory = new ResponseFactory();
    }

    /**
     * @test
     * @dataProvider createsErrorResponseExamples
     * @param ChecklistOperationError $error
     * @param array|mixed[] $expectedResponseData
     * @param int $expectedStatusCode
     */
    public function createsErrorResponse(
        ChecklistOperationError $error,
        array $expectedResponseData,
        int $expectedStatusCode
    ): void
    {
        $response = $this->factory->createErrorResponse($error);

        expect($response)->isInstanceOf(JsonResponse::class);
        expect($response->getStatusCode())->equals($expectedStatusCode);

        $data = json_decode($response->getContent(), true);
        expect($data)->equals($expectedResponseData);
    }

    /**
     * @test
     */
    public function throwsOnCreateErrorResponseWithoutError(): void
    {
        $this->expectException(AssertionFailedException::class);

        $this->factory->createErrorResponse(ChecklistOperationError::None());
    }

    /**
     * @test
     */
    public function createsResponse(): void
    {
        $response = $this->factory->createResponse(new TestResult(), Response::HTTP_NO_CONTENT);

        expect($response)->isInstanceOf(JsonResponse::class);
        expect($response->getStatusCode())->equals(Response::HTTP_NO_CONTENT);

        $responseData = json_decode($response->getContent(), true);
        expect($responseData)->equals(["data" => "test data", "error" => null]);
    }

    /**
     * @test
     */
    public function createsResponseWithDefaultStatusCode(): void
    {
        $response = $this->factory->createResponse(new TestResult());

        expect($response)->isInstanceOf(JsonResponse::class);
        expect($response->getStatusCode())->equals(Response::HTTP_OK);

        $responseData = json_decode($response->getContent(), true);
        expect($responseData)->equals(["data" => "test data", "error" => null]);
    }

    /**
     * @test
     */
    public function createsEmptyResponse(): void
    {
        $response = $this->factory->createEmptyResponse(Response::HTTP_ACCEPTED);

        expect($response)->isInstanceOf(JsonResponse::class);
        expect($response->getContent())->isEmpty();
        expect($response->getStatusCode())->equals(Response::HTTP_ACCEPTED);
    }

    /**
     * @test
     */
    public function createsEmptyResponseWithDefaultStatusCode(): void
    {
        $response = $this->factory->createEmptyResponse();

        expect($response)->isInstanceOf(JsonResponse::class);
        expect($response->getContent())->isEmpty();
        expect($response->getStatusCode())->equals(Response::HTTP_CREATED);
    }

    /**
     * @return array|array[]
     */
    public function createsErrorResponseExamples(): array
    {
        return [
            [
                ChecklistOperationError::TodoNotFound(),
                [
                    "data" => null,
                    "error" => ["message" => "TodoNotFound", "code" => "TodoNotFound", "violations" => []],
                ],
                Response::HTTP_NOT_FOUND,
            ],
            [
                ChecklistOperationError::ChecklistNotFound(),
                [
                    "data" => null,
                    "error" => ["message" => "ChecklistNotFound", "code" => "ChecklistNotFound", "violations" => [],],
                ],
                Response::HTTP_NOT_FOUND,
            ],
            [
                new TestOperationError(TestOperationError::InternalServerError),
                [
                    "data" => null,
                    "error" => ["message" => "InternalServerError", "code" => "InternalServerError", "violations" => []],
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR,
            ],
        ];
    }
}

class TestOperationError extends ChecklistOperationError
{
    const InternalServerError = "InternalServerError";
}

class TestResult implements \JsonSerializable
{
    public function jsonSerialize()
    {
        return "test data";
    }
}