<?php

namespace Tests\DemoCode\TodoList\Unit\Entry\Api\AddTodo;

use DemoCode\TodoList\Application\Domain\Todo;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\AddTodo;
use DemoCode\TodoList\Entry\Api\AddTodo\AddTodoRequest;
use DemoCode\TodoList\Entry\Api\AddTodo\RequestHandler;
use DemoCode\TodoList\Entry\Api\ResponseFactory;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RequestHandlerTest extends TestCase
{
    /**
     * @var ObjectProphecy|AddTodo\Interactor
     */
    private $interactor;
    /**
     * @var ObjectProphecy|UrlGeneratorInterface
     */
    private $urlGenerator;
    /**
     * @var ObjectProphecy|ResponseFactory
     */
    private $responseFactory;
    /**
     * @var RequestHandler
     */
    private $handler;

    protected function setUp(): void
    {
        $this->interactor = $this->prophesize(AddTodo\Interactor::class);
        $this->responseFactory = $this->prophesize(ResponseFactory::class);
        $this->urlGenerator = $this->prophesize(UrlGeneratorInterface::class);
        $this->handler = new RequestHandler(
            $this->interactor->reveal(),
            $this->responseFactory->reveal(),
            $this->urlGenerator->reveal()
        );
    }

    /**
     * @test
     */
    public function addTodo(): void
    {
        $todo = Todo::createNew("todo title", true, 3);
        $interactorResult = AddTodo\AddTodoResult::createNew($todo);
        $this->interactor->addTodo(Argument::type(AddTodo\AddTodoRequest::class))
            ->shouldBeCalled()
            ->will(function (array $args) use ($interactorResult) {
                expect($args)->hasKey(0);
                /** @var AddTodo\AddTodoRequest $request */
                $request = $args[0];
                expect($request)->isInstanceOf(AddTodo\AddTodoRequest::class);
                expect($request->getTitle())->equals("todo title");
                expect($request->isCompleted())->true();
                expect($request->getPosition())->same(3);

                return $interactorResult;
            });

        $httpResponse = new JsonResponse();
        $this->responseFactory->createEmptyResponse(Response::HTTP_CREATED)
            ->shouldBeCalled()
            ->willReturn($httpResponse);

        $this->urlGenerator->generate("api.checklist.remove_todo", [
            "checklistId" => "12345",
            "todoId" => $todo->getId()
        ])->shouldBeCalled()
            ->willReturn("resource url");

        $handlerResponse = $this->handler->addTodo(AddTodoRequest::createNew("12345", "todo title", true, 3));

        expect($handlerResponse)->same($httpResponse);
        expect($handlerResponse->headers->get("Location"))->equals("resource url");
    }

    /**
     * @test
     */
    public function handlesResultWithError(): void
    {
        $this->interactor->addTodo(Argument::type(AddTodo\AddTodoRequest::class))
            ->shouldBeCalled()
            ->willReturn(AddTodo\AddTodoResult::createWithError(ChecklistOperationError::ChecklistNotFound()));

        $httpResponse = new JsonResponse();
        $this->responseFactory->createErrorResponse(ChecklistOperationError::ChecklistNotFound())
            ->shouldBeCalled()
            ->willReturn($httpResponse);

        $handlerResponse = $this->handler->addTodo(AddTodoRequest::createNew("12345", "todo title", true, 3));

        expect($handlerResponse)->same($httpResponse);
    }
}
