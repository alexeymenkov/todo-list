<?php

namespace Tests\DemoCode\TodoList\Unit\Entry\Api\AddTodo;

use DemoCode\TodoList\Entry\Api\AddTodo\AddTodoRequest;
use PHPUnit\Framework\TestCase;

class AddTodoRequestTest extends TestCase
{
    /**
     * @test
     */
    public function mergeFromJsonString(): void
    {
        $json = json_encode([
            "checklistId" => "123",
            "title" => "a",
            "completed" => false,
            "position" => 12,
        ]);

        $request = new AddTodoRequest();
        $request->mergeFromJsonString($json);
        expect($request->getChecklistId())->equals("123");
        expect($request->getTitle())->equals("a");
        expect($request->isCompleted())->false();
        expect($request->getPosition())->same(12);
    }

    /**
     * @test
     */
    public function canBeCreatedWithNullValues(): void
    {
        $request = new AddTodoRequest();
        $request->mergeFromJsonString("[]");

        expect($request->getChecklistId())->null();
        expect($request->getTitle())->null();
        expect($request->getPosition())->null();
        expect($request->isCompleted())->null();
    }
}
