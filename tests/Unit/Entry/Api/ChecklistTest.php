<?php

namespace Tests\DemoCode\TodoList\Unit\Entry\Api;

use DemoCode\TodoList\Application\Domain;
use DemoCode\TodoList\Entry\Api\Checklist;
use PHPUnit\Framework\TestCase;

class ChecklistTest extends TestCase
{

    /**
     * @test
     */
    public function jsonSerialize(): void
    {
        $domainChecklist = Domain\Checklist::createNew("title");
        $todo = Domain\Todo::createNew("todo", false, 0);
        $domainChecklist->addTodo($todo);

        $json = json_encode(new Checklist($domainChecklist));
        $data = json_decode($json, true);

        expect($data)->hasKey("id");
        expect($data)->hasKey("title");
        expect($data)->hasKey("createdAt");
        expect($data)->hasKey("updatedAt");
        expect($data)->hasKey("todoList");
        expect($data["id"])->equals($domainChecklist->getId()->toString());
        expect($data["title"])->equals($domainChecklist->getTitle());
        expect($data["createdAt"])->equals($domainChecklist->getCreatedAt()->format(\DateTime::ATOM));
        expect($data["updatedAt"])->equals($domainChecklist->getUpdatedAt()->format(\DateTime::ATOM));
        expect($data["todoList"])->array();
        expect($data["todoList"])->hasKey(0);
        expect($data["todoList"][0])->equals([
            "id" => $todo->getId(),
            "title" => $todo->getTitle(),
            "position" => $todo->getPosition(),
            "isCompleted" => $todo->isCompleted(),
            "createdAt" => $todo->getCreatedAt()->format(\DateTime::ATOM),
            "updatedAt" => $todo->getUpdatedAt()->format(\DateTime::ATOM),
        ]);
    }

}
