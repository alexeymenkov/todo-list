<?php

namespace Tests\DemoCode\TodoList\Unit\Entry\Api\CreateChecklist;

use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\UseCase\CreateChecklist;
use DemoCode\TodoList\Entry\Api\CreateChecklist\CreateChecklistRequest;
use DemoCode\TodoList\Entry\Api\CreateChecklist\RequestHandler;
use DemoCode\TodoList\Entry\Api\ResponseFactory;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RequestHandlerTest extends TestCase
{
    /**
     * @var ObjectProphecy|CreateChecklist\Interactor
     */
    private $interactor;
    /**
     * @var ObjectProphecy|ResponseFactory
     */
    private $responseFactory;
    /**
     * @var ObjectProphecy|UrlGeneratorInterface
     */
    private $urlGenerator;
    /**
     * @var RequestHandler
     */
    private $handler;

    protected function setUp(): void
    {
        $this->interactor = $this->prophesize(CreateChecklist\Interactor::class);
        $this->responseFactory = $this->prophesize(ResponseFactory::class);
        $this->urlGenerator = $this->prophesize(UrlGeneratorInterface::class);
        $this->handler = new RequestHandler(
            $this->interactor->reveal(),
            $this->responseFactory->reveal(),
            $this->urlGenerator->reveal()
        );
    }

    public function testCreateChecklist(): void
    {
        $checklist = Checklist::createNew("title");
        $this->interactor->createChecklist(Argument::type(CreateChecklist\CreateChecklistRequest::class))
            ->shouldBeCalled()
            ->will(function (array $args) use ($checklist) {
                expect($args)->hasKey(0);
                /** @var CreateChecklist\CreateChecklistRequest $request */
                $request = $args[0];
                expect($request->getTitle())->equals("title");

                return CreateChecklist\CreateChecklistResult::create($checklist);
            });

        $this->urlGenerator->generate("api.checklist.find", ["checklistId" => $checklist->getId()->toString()])
            ->shouldBeCalled()
            ->willReturn("resource url");

        $jsonResponse = new JsonResponse();
        $this->responseFactory->createEmptyResponse()
            ->shouldBeCalled()
            ->willReturn($jsonResponse);

        $response = $this->handler->createChecklist(CreateChecklistRequest::createNew("title"));

        expect($response)->same($jsonResponse);
        expect($response->headers->get("Location"))->equals("resource url");
    }
}
