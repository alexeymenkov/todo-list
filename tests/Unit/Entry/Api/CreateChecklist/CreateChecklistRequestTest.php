<?php

namespace Tests\DemoCode\TodoList\Unit\Entry\Api\CreateChecklist;

use DemoCode\TodoList\Entry\Api\CreateChecklist\CreateChecklistRequest;
use PHPUnit\Framework\TestCase;

class CreateChecklistRequestTest extends TestCase
{
    /**
     * @test
     */
    public function mergeFromJsonString(): void
    {
        $json = json_encode(["title" => "test"]);

        $request = new CreateChecklistRequest();
        $request->mergeFromJsonString($json);

        expect($request->getTitle())->equals("test");
    }

    /**
     * @test
     */
    public function canBeCreatedWithNullValues(): void
    {
        $request = new CreateChecklistRequest();
        $json = json_encode(["test" => "title"]);

        $request->mergeFromJsonString($json);
        expect($request->getTitle())->null();
    }
}
