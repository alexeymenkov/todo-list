<?php

namespace Tests\DemoCode\TodoList\Unit\Entry\Api\UpdateTodo;

use DemoCode\TodoList\Application\Domain;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\UseCase\UpdateTodo;
use DemoCode\TodoList\Entry\Api\ResponseFactory;
use DemoCode\TodoList\Entry\Api\Todo;
use DemoCode\TodoList\Entry\Api\UpdateTodo\RequestHandler;
use DemoCode\TodoList\Entry\Api\UpdateTodo\UpdateTodoRequest;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\HttpFoundation\JsonResponse;

class RequestHandlerTest extends TestCase
{
    /**
     * @var ObjectProphecy|UpdateTodo\Interactor
     */
    private $interactor;
    /**
     * @var ObjectProphecy|ResponseFactory
     */
    private $responseFactory;
    /**
     * @var RequestHandler
     */
    private $requestHandler;

    protected function setUp(): void
    {
        $this->interactor = $this->prophesize(UpdateTodo\Interactor::class);
        $this->responseFactory = $this->prophesize(ResponseFactory::class);
        $this->requestHandler = new RequestHandler(
            $this->interactor->reveal(),
            $this->responseFactory->reveal()
        );
    }

    public function testUpdateTodo(): void
    {
        $interactorResult = UpdateTodo\UpdateTodoResult::create(Domain\Todo::createNew("a", false, 1));
        $this->interactor->updateTodo(Argument::type(UpdateTodo\UpdateTodoRequest::class))
            ->shouldBeCalled()
            ->will(function (array $args) use ($interactorResult) {
                expect($args)->hasKey(0);
                /** @var UpdateTodo\UpdateTodoRequest $domainRequest */
                $domainRequest = $args[0];
                expect($domainRequest)->isInstanceOf(UpdateTodo\UpdateTodoRequest::class);

                expect($domainRequest->getChecklistId()->toString())->equals("123");
                expect($domainRequest->getTodoId()->toString())->equals("34");
                expect($domainRequest->getTitle())->equals("a");
                expect($domainRequest->isCompleted())->true();
                expect($domainRequest->getPosition())->same(1);

                return $interactorResult;
            });

        $response = new JsonResponse();
        $this->responseFactory->createResponse(Argument::type(Todo::class))
            ->shouldBeCalled()
            ->will(function (array $args) use ($response, $interactorResult) {
                 expect($args)->hasKey(0);
                /** @var Todo $apiTodo */
                $apiTodo = $args[0];
                expect($apiTodo)->isInstanceOf(Todo::class);
                expect($apiTodo)->equals(new Todo($interactorResult->getTodo()));

                return $response;
            });

        $result = $this->requestHandler->updateTodo("123", "34", UpdateTodoRequest::createNew("a", true, 1));

        expect($result)->same($response);
    }

    /**
     * @test
     */
    public function handlesResultWithError(): void
    {
        $this->interactor->updateTodo(Argument::type(UpdateTodo\UpdateTodoRequest::class))
            ->shouldBeCalled()
            ->willReturn(UpdateTodo\UpdateTodoResult::createWithError(ChecklistOperationError::ChecklistNotFound()));

        $response = new JsonResponse();
        $this->responseFactory->createErrorResponse(ChecklistOperationError::ChecklistNotFound())
            ->shouldBeCalled()
            ->willReturn($response);

        $result = $this->requestHandler->updateTodo("123", "34", UpdateTodoRequest::createNew("a", true, 1));

        expect($result)->same($response);
    }
}
