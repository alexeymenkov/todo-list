<?php

namespace Tests\DemoCode\TodoList\Unit\Entry\Api;

use DemoCode\TodoList\Entry\Api\ApiResponse;
use DemoCode\TodoList\Entry\Api\Checklist;
use DemoCode\TodoList\Entry\Api\ConstraintViolation;
use PHPUnit\Framework\TestCase;
use DemoCode\TodoList\Application\Domain;

class ApiResponseTest extends TestCase
{
    /**
     * @test
     * @dataProvider jsonSerializeExamples
     */
    public function testJsonSerialize(ApiResponse $apiResponse, string $expectedJson): void
    {
        $json = json_encode($apiResponse);
        expect($json)->equals($expectedJson);
    }

    /**
     * @return array|array[]
     */
    public function jsonSerializeExamples(): array
    {
        return [
            [
                ApiResponse::createNew(new ConstraintViolation("path", "message")),
                "{\"error\":null,\"data\":{\"propertyPath\":\"path\",\"message\":\"message\"}}"
            ]
        ];
    }

    /**
     * @test
     */
    public function throwsWhenEmpty(): void
    {
        $this->expectException(\LogicException::class);

        $refClass = new \ReflectionClass(ApiResponse::class);
        $obj = $refClass->newInstanceWithoutConstructor();

        json_encode($obj);
    }
}