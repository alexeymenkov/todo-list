<?php

namespace Tests\DemoCode\TodoList\Integration\Infrastructure\Storage;

use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Domain\Todo;
use DemoCode\TodoList\Application\Domain\TodoId;
use DemoCode\TodoList\Infrastructure\Storage\CacheStorage;
use Doctrine\Common\Cache\ArrayCache;
use PHPUnit\Framework\TestCase;

class CacheStorageTest extends TestCase
{
    /**
     * @var ArrayCache
     */
    private $cache;
    /**
     * @var CacheStorage
     */
    private $storage;

    public function setUp(): void
    {
        $this->cache = new ArrayCache();
        $this->storage = new CacheStorage($this->cache);
    }

    /**
     * @test
     */
    public function saveChecklist(): void
    {
        $checklist = $this->createChecklist("a");

        $this->storage->saveChecklist($checklist);

        expect($this->cache->contains("a"))->true();
        expect($this->cache->fetch("a"))->equals($checklist);
    }

    /**
     * @test
     */
    public function findChecklist(): void
    {
        $checklist = $this->createChecklist("b");

        $this->cache->save("b", $checklist);

        $result = $this->storage->findChecklist(new ChecklistId("b"));

        expect($result)->isInstanceOf(Checklist::class);
        expect($result)->equals($checklist);
    }

    /**
     * @test
     */
    public function returnsNullWhenChecklistNotFound(): void
    {
        $this->cache->save("b", $this->createChecklist("b"));

        $result = $this->storage->findChecklist(new ChecklistId("c"));

        expect($result)->null();
    }

    /**
     * @test
     */
    public function deleteChecklist(): void
    {
        $this->cache->save("c", $this->createChecklist("c"));

        $this->storage->deleteChecklist(new ChecklistId("c"));
        expect($this->cache->contains("c"))->false();
    }

    private function createChecklist(string $id): Checklist
    {
        return new Checklist(
            new ChecklistId($id),
            "b",
            [
                new Todo(new TodoId("t1"), "title1", false, 0,  new \DateTimeImmutable(), new \DateTimeImmutable()),
                new Todo(new TodoId("t2"), "title2", false, 1, new \DateTimeImmutable(), new \DateTimeImmutable()),
            ],
            new \DateTimeImmutable(),
            new \DateTimeImmutable()
        );
    }
}
