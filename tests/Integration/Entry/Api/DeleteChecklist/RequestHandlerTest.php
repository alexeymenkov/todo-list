<?php

namespace Tests\DemoCode\TodoList\Integration\Entry\Api\DeleteChecklist;

use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use Doctrine\Common\Cache\CacheProvider;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Tests\DemoCode\TodoList\FIxtures\Helpers\ChecklistHelper;

/**
 * @coversNothing
 */
class RequestHandlerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    protected function setUp(): void
    {
        $this->client = self::createClient();
    }

    protected function tearDown(): void
    {
        /** @var CacheProvider $cache */
        $cache = $this->client->getContainer()->get("filesystem_cache");
        $cache->deleteAll();

        parent::tearDown();
    }

    /**
     * @test
     */
    public function deleteChecklist(): void
    {
        ChecklistHelper::loadFixtures($this->client, "12345");

        $this->client->request("DELETE", "/api/checklists/12345");

        expect($this->client->getResponse()->getStatusCode())->equals(Response::HTTP_NO_CONTENT);
        expect($this->client->getResponse()->getContent())->isEmpty();

        /** @var ChecklistRepository $checklistRepository */
        $checklistRepository = $this->client->getContainer()->get(ChecklistRepository::class);

        $checklist = $checklistRepository->findChecklist(new ChecklistId("12345"));
        expect($checklist)->null();
    }
}
