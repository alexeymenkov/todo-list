<?php

namespace Tests\DemoCode\TodoList\Integration\Entry\Api\RemoveTodo;

use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use Doctrine\Common\Cache\CacheProvider;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Tests\DemoCode\TodoList\FIxtures\Helpers\ChecklistHelper;

/**
 * @coversNothing
 */
class RequestHandlerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    protected function setUp(): void
    {
        $this->client = self::createClient();
    }

    protected function tearDown(): void
    {
        /** @var CacheProvider $cache */
        $cache = $this->client->getContainer()->get("filesystem_cache");
        $cache->deleteAll();

        parent::tearDown();
    }

    /**
     * @test
     */
    public function removeTodo(): void
    {
        $checklist = ChecklistHelper::loadFixtures($this->client, "b");

        $todoId = $checklist->getTodoList()[0]->getId()->toString();
        $this->client->request("DELETE", "/api/checklists/b/todos/{$todoId}");

        expect($this->client->getResponse()->getStatusCode())->equals(Response::HTTP_NO_CONTENT);
        expect($this->client->getResponse()->getContent())->isEmpty();

        /** @var ChecklistRepository $repository */
        $repository = $this->client->getContainer()->get(ChecklistRepository::class);
        $updatedChecklist = $repository->findChecklist(new ChecklistId("b"));

        expect($updatedChecklist->getTodoList())->isEmpty();
    }
}
