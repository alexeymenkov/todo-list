<?php

namespace Tests\DemoCode\TodoList\Integration\Entry\Api\AddTodo;

use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use Doctrine\Common\Cache\CacheProvider;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Tests\DemoCode\TodoList\FIxtures\Helpers\ChecklistHelper;

/**
 * @coversNothing
 */
class RequestHandlerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    protected function setUp(): void
    {
        $this->client = self::createClient();
    }

    protected function tearDown(): void
    {
        /** @var CacheProvider $cache */
        $cache = $this->client->getContainer()->get("filesystem_cache");
        $cache->deleteAll();

        parent::tearDown();
    }


    /**
     * @test
     */
    public function addTodo(): void
    {
        ChecklistHelper::loadFixtures($this->client, "a");

        $requestData = [
            "title" => "new todo title",
            "completed" => true,
            "position" => 10,
            "checklistId" => "a",
        ];
        $this->client->request("POST", "/api/checklists/a/todos", [], [], [], json_encode($requestData));

        expect($this->client->getResponse()->getStatusCode())->equals(Response::HTTP_CREATED);
        expect($this->client->getResponse()->getContent())->isEmpty();
        expect($this->client->getResponse()->headers->has("Location"))->true();
        expect($this->client->getResponse()->headers->get("Location"))
            ->stringContainsString("/api/checklists/a/todos/");

        /** @var ChecklistRepository $repository */
        $repository = $this->client->getContainer()->get(ChecklistRepository::class);

        $updatedChecklist = $repository->findChecklist(new ChecklistId("a"));
        expect($updatedChecklist->getTodoList())->count(2);
        expect($updatedChecklist->getTodoList())->hasKey(1);

        $newTodo = $updatedChecklist->getTodoList()[1];
        expect($newTodo->getTitle())->equals("new todo title");
        expect($newTodo->getPosition())->same(10);
        expect($newTodo->isCompleted())->true();
    }
}
