<?php

namespace Tests\DemoCode\TodoList\Integration\Entry\Api\AddTodo;

use DemoCode\TodoList\Entry\Api\AddTodo\AddTodoRequest;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidateAddTodoRequestTest extends KernelTestCase
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->validator = self::$kernel->getContainer()->get("validator");
    }

    /**
     * @dataProvider validationExamples
     * @param array|mixed[] $data
     * @param array|string[] $expectedErrorMessages
     */
    public function testRequestValidation(array $data, array $expectedErrorMessages): void
    {
        $request = new AddTodoRequest();
        $request->mergeFromJsonString(json_encode($data));

        /** @var ConstraintViolationInterface[] $errors */
        $errors = $this->validator->validate($request);

        $errorMessages = [];
        foreach ($errors as $error) {
            $errorMessages[$error->getPropertyPath()] = $error->getMessage();
        }

        expect($errorMessages)->equals($expectedErrorMessages);
    }

    /**
     * @return array|array[]
     */
    public function validationExamples(): array
    {
        return [
            [
                ["checklistId" => "123", "title" => "a", "completed" => true, "position" => 0],
                []
            ],
            [
                ["checklistId" => "", "title" => "", "completed" => false, "position" => -1],
                ["checklistId" => "This value should not be blank.", "title" => "This value should not be blank."]
            ],
            [
                [],
                [
                    "title" => "This value should not be null.",
                    "checklistId" => "This value should not be null.",
                    "completed" => "This value should not be null.",
                    "position" => "This value should not be null.",
                ]
            ],
            [
                ["checklistId" => 10, "title" => 12, "completed" => "false", "position" => "-1"],
                [
                    "checklistId" => "This value should be of type string.",
                    "title" => "This value should be of type string.",
                    "completed" => "This value should be of type bool.",
                    "position" => "This value should be of type integer.",
                ]
            ]
        ];
    }
}
