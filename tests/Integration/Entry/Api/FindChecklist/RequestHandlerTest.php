<?php declare(strict_types=1);


namespace Tests\DemoCode\TodoList\Integration\Entry\Api\FindChecklist;


use Doctrine\Common\Cache\CacheProvider;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Tests\DemoCode\TodoList\FIxtures\Helpers\ChecklistHelper;

/**
 * @coversNothing
 */
class RequestHandlerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    protected function setUp(): void
    {
        $this->client = self::createClient();
    }

    protected function tearDown(): void
    {
        /** @var CacheProvider $cache */
        $cache = $this->client->getContainer()->get("filesystem_cache");
        $cache->deleteAll();

        parent::tearDown();
    }

    /**
     * @test
     */
    public function findChecklist(): void
    {
        ChecklistHelper::loadFixtures($this->client, "1234");

        $this->client->request("GET", "/api/checklists/1234");
        $response = $this->client->getResponse();

        expect($response->getStatusCode())->equals(Response::HTTP_OK);
        $jsonResponse = json_decode($response->getContent(), true);

        expect($jsonResponse)->hasKey("error");
        expect($jsonResponse)->hasKey("data");
        expect($jsonResponse["data"])->hasKey("id");
        expect($jsonResponse["data"]["id"])->equals("1234");
    }

}