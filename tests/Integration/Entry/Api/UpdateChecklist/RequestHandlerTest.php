<?php

namespace Tests\DemoCode\TodoList\Integration\Entry\Api\UpdateChecklist;


use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use Doctrine\Common\Cache\CacheProvider;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Tests\DemoCode\TodoList\FIxtures\Helpers\ChecklistHelper;

/**
 * @coversNothing
 */
class RequestHandlerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    protected function setUp(): void
    {
        $this->client = self::createClient();
    }

    protected function tearDown(): void
    {
        /** @var CacheProvider $cache */
        $cache = $this->client->getContainer()->get("filesystem_cache");
        $cache->deleteAll();

        parent::tearDown();
    }

    /**
     * @test
     */
    public function updateChecklist(): void
    {
        ChecklistHelper::loadFixtures($this->client, "a");

        $data = ["title" => "new title"];
        $this->client->request("PUT", "/api/checklists/a", [], [], [], json_encode($data));

        expect($this->client->getResponse()->getStatusCode())->equals(Response::HTTP_OK);
        expect($this->client->getResponse()->getContent())->notEmpty();

        $checklistData = json_decode($this->client->getResponse()->getContent(), true);
        expect($checklistData)->hasKey("data");
        expect($checklistData["data"])->hasKey("title");
        expect($checklistData["data"]["title"])->equals("new title");

        /** @var ChecklistRepository $repository */
        $repository = $this->client->getContainer()->get(ChecklistRepository::class);
        $updatedChecklist = $repository->findChecklist(new ChecklistId("a"));

        expect($updatedChecklist)->isInstanceOf(Checklist::class);
        expect($updatedChecklist->getTitle())->equals("new title");
    }
}
