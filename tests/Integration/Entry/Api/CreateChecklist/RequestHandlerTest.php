<?php

namespace Tests\DemoCode\TodoList\Integration\Entry\Api\CreateChecklist;

use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @coversNothing
 */
class RequestHandlerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    protected function setUp(): void
    {
        $this->client = self::createClient();
    }

    /**
     * @test
     */
    public function createChecklist(): void
    {
        $this->client->request("POST", "/api/checklists", [], [], [], json_encode(["title" => "test"]));

        expect($this->client->getResponse()->getStatusCode())->equals(Response::HTTP_CREATED);
        expect($this->client->getResponse()->getContent())->isEmpty();
        $location = $this->client->getResponse()->headers->get("Location");
        expect($location)->stringContainsString("/api/checklists");

        /** @var ChecklistRepository $checklistRepository */
        $checklistRepository = $this->client->getContainer()->get(ChecklistRepository::class);
        /** @var Checklist $newChecklist */
        $newChecklist = $checklistRepository->findChecklist(new ChecklistId(explode("/", $location)[3]));

        expect($newChecklist)->isInstanceOf(Checklist::class);
        expect($newChecklist->getTitle())->equals("test");
        expect($newChecklist->getTodoList())->isEmpty();
    }

    /**
     * @test
     */
    public function returnsBadRequestOnValidationErrors(): void
    {
        $this->client->request("POST", "/api/checklists", [], [], [], json_encode(["title" => ""]));

        expect($this->client->getResponse()->getStatusCode())->equals(Response::HTTP_BAD_REQUEST);
        expect($this->client->getResponse()->getContent())->notEmpty();

        $data = json_decode($this->client->getResponse()->getContent(), true);
        expect($data)->array();
        expect($data)->hasKey("error");
        expect($data["error"])->hasKey("violations");
        expect($data["error"]["violations"])->array();
        expect($data["error"]["violations"])->count(1);
        expect($data["error"]["violations"])->hasKey(0);
        expect($data["error"]["violations"][0])->equals(["propertyPath" => "title", "message" => "This value should not be blank."]);
    }
}
