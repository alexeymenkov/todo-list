<?php

namespace Tests\DemoCode\TodoList\Integration\Entry\Api\UpdateTodo;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Tests\DemoCode\TodoList\FIxtures\Helpers\ChecklistHelper;

/**
 * @coversNothing
 */
class RequestHandlerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    protected function setUp(): void
    {
        $this->client = self::createClient();
    }

    /**
     * @test
     */
    public function updateTodo(): void
    {
        $checklist = ChecklistHelper::loadFixtures($this->client, "12345");
        $todoId = $checklist->getTodoList()[0]->getId()->toString();

        $data = ["title" => "test", "completed" => true, "position" => 1];
        $this->client->request("PUT", "/api/checklists/12345/todos/{$todoId}", [], [], [], json_encode($data));

        expect($this->client->getResponse()->getStatusCode())->equals(Response::HTTP_OK);
        expect($this->client->getResponse()->getContent())->notEmpty();

        $data = json_decode($this->client->getResponse()->getContent(), true);
        expect($data)->hasKey("data");
        expect($data["data"])->array();
        expect($data["data"])->hasKey("id");
        expect($data["data"]["id"])->equals($todoId);
        expect($data["data"])->hasKey("title");
        expect($data["data"]["title"])->equals("test");
    }
}
