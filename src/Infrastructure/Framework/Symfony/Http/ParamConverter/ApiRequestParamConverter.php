<?php declare(strict_types=1);


namespace DemoCode\TodoList\Infrastructure\Framework\Symfony\Http\ParamConverter;


use DemoCode\TodoList\Entry\Api\ApiRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class ApiRequestParamConverter implements ParamConverterInterface
{
    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @return bool
     * @throws \ReflectionException
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        /** @var class-string $class */
        $class = $configuration->getClass();

        $refClass = new \ReflectionClass($class);
        $object = $refClass->newInstanceWithoutConstructor();

        if (!$object instanceof ApiRequest) {
            return false;
        }

        $object->mergeFromJsonString((string) $request->getContent());

        $request->attributes->set($configuration->getName(), $object);

        return true;
    }

    /**
     * @param ParamConverter $configuration
     * @return bool
     * @throws \ReflectionException
     */
    public function supports(ParamConverter $configuration)
    {
        /** @var class-string $class */
        $class = $configuration->getClass();
        if (empty($class)) {
            return false;
        }

        $refClass = new \ReflectionClass($class);

        return $refClass->implementsInterface(ApiRequest::class);
    }
}