<?php declare(strict_types=1);


namespace DemoCode\TodoList\Infrastructure\Framework\Symfony\Http\Middleware\ValidateApiRequest;


use Assert\Assertion;
use DemoCode\TodoList\Entry\Api\ConstraintViolation;

class RequestValidationException extends \Exception
{
    /**
     * @var ConstraintViolation[]
     */
    private $constraintViolationList;

    /**
     * RequestValidationException constructor.
     * @param ConstraintViolation[] $constraintViolationList
     */
    public function __construct(array $constraintViolationList)
    {
        parent::__construct("Request validation error");
        Assertion::allIsInstanceOf($constraintViolationList, ConstraintViolation::class);
        $this->constraintViolationList = $constraintViolationList;
    }

    /**
     * @return ConstraintViolation[]
     */
    public function getConstraintViolationList(): array
    {
        return $this->constraintViolationList;
    }
}