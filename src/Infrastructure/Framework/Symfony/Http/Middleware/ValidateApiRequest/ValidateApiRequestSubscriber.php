<?php declare(strict_types=1);


namespace DemoCode\TodoList\Infrastructure\Framework\Symfony\Http\Middleware\ValidateApiRequest;


use DemoCode\TodoList\Entry\Api\ApiRequest;
use DemoCode\TodoList\Entry\Api\ApiResponse;
use DemoCode\TodoList\Entry\Api\ConstraintViolation;
use DemoCode\TodoList\Entry\Api\ResponseError;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerArgumentsEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidateApiRequestSubscriber implements EventSubscriberInterface
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ApiSubscriber constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @return array|string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER_ARGUMENTS => "onControllerArguments",
            KernelEvents::EXCEPTION => "onException",            
        ];
    }

    /**
     * @param FilterControllerArgumentsEvent $event
     * @throws RequestValidationException
     */
    public function onControllerArguments(FilterControllerArgumentsEvent $event): void
    {
        foreach ($event->getArguments() as $argument) {
            if (!$argument instanceof ApiRequest) {
                continue;
            }

            $errors = $this->validator->validate($argument);

            if ($errors->count() === 0) {
                continue;
            }

            $violations = [];
            foreach ($errors as $error) {
                $violations[] = new ConstraintViolation($error->getPropertyPath(), $error->getMessage());
            }

            throw new RequestValidationException($violations);
        }
    }

    public function onException(GetResponseForExceptionEvent $event): void
    {
        $exception = $event->getException();
        if (!$exception instanceof RequestValidationException) {
            return;
        }

        $apiResponse = ApiResponse::createWithError(
            new ResponseError("Validation error", "validation_error", $exception->getConstraintViolationList())
        );

        $event->setResponse(new JsonResponse($apiResponse, Response::HTTP_BAD_REQUEST));
    }
}