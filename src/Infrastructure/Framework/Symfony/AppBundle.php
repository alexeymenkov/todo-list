<?php declare(strict_types=1);


namespace DemoCode\TodoList\Infrastructure\Framework\Symfony;


use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{

}