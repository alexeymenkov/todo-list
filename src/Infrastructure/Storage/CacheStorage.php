<?php declare(strict_types=1);


namespace DemoCode\TodoList\Infrastructure\Storage;


use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Service\ChecklistPersister;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use DemoCode\TodoList\Application\UseCase\DeleteChecklist\ChecklistDeleter;
use Doctrine\Common\Cache\Cache;

class CacheStorage implements ChecklistRepository, ChecklistPersister, ChecklistDeleter
{
    /**
     * @var Cache
     */
    private $cache;

    /**
     * CacheStorage constructor.
     * @param Cache $cache
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    public function saveChecklist(Checklist $checklist): void
    {
        $this->cache->save((string) $checklist->getId(), $checklist);
    }

    public function findChecklist(ChecklistId $checklistId): ?Checklist
    {
        $result = $this->cache->fetch((string) $checklistId);

        return $result ?: null;
    }

    public function deleteChecklist(ChecklistId $checklistId): void
    {
        $this->cache->delete((string) $checklistId);
    }
}