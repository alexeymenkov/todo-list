<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api;


use Assert\Assertion;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ResponseFactory
{
    public function createErrorResponse(ChecklistOperationError $error): Response
    {
        Assertion::notEq($error, ChecklistOperationError::None());

        $apiResponse = ApiResponse::createWithError(new ResponseError($error->getValue(), $error->getKey()));

        return new JsonResponse($apiResponse, $this->getStatusCode($error));
    }

    public function createResponse(\JsonSerializable $result, int $statusCode = Response::HTTP_OK): Response
    {
        return new JsonResponse(ApiResponse::createNew($result), $statusCode);
    }

    public function createEmptyResponse(int $statusCode = Response::HTTP_CREATED): Response
    {
        return (new JsonResponse(null, $statusCode))->setContent(null);
    }

    private function getStatusCode(ChecklistOperationError $error): int
    {
        $statusCode = 500;

        switch (true):
            case $error->equals(ChecklistOperationError::ChecklistNotFound()):
            case $error->equals(ChecklistOperationError::TodoNotFound()):
                $statusCode = 404; break;
        endswitch;

        return $statusCode;
    }
}