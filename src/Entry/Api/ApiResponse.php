<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api;


class ApiResponse implements \JsonSerializable
{
    /**
     * @var ResponseError|null
     */
    private $error;
    /**
     * @var \JsonSerializable|null
     */
    private $data;

    private function __construct()
    {
    }

    public static function createNew(\JsonSerializable $data): self
    {
        $response = new self();
        $response->data = $data;

        return $response;
    }

    public static function createWithError(ResponseError $responseError): self
    {
        $response = new self();
        $response->error = $responseError;

        return $response;
    }

    public function jsonSerialize()
    {
        if ($this->isEmpty()) {
            throw new \LogicException();
        }

        return [
            "error" => $this->error,
            "data" => $this->data,
        ];
    }

    /**
     * @return bool
     */
    private function isEmpty(): bool
    {
        return !$this->data && !$this->error;
    }
}