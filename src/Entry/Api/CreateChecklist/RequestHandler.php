<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api\CreateChecklist;


use DemoCode\TodoList\Application\UseCase\CreateChecklist;
use DemoCode\TodoList\Entry\Api\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RequestHandler
{
    /**
     * @var CreateChecklist\Interactor
     */
    private $interactor;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * RequestHandler constructor.
     * @param CreateChecklist\Interactor $interactor
     * @param ResponseFactory $responseFactory
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(
        CreateChecklist\Interactor $interactor,
        ResponseFactory $responseFactory,
        UrlGeneratorInterface $urlGenerator
    )
    {
        $this->interactor = $interactor;
        $this->responseFactory = $responseFactory;
        $this->urlGenerator = $urlGenerator;
    }

    public function createChecklist(CreateChecklistRequest $request): Response
    {
        $domainRequest = new CreateChecklist\CreateChecklistRequest($request->getTitle());
        $result = $this->interactor->createChecklist($domainRequest);

        $response = $this->responseFactory->createEmptyResponse();
        $resourceUrl = $this->urlGenerator->generate(
            "api.checklist.find",
            ["checklistId" => $result->getChecklist()->getId()]
        );

        $response->headers->set("Location", $resourceUrl);

        return $response;
    }
}