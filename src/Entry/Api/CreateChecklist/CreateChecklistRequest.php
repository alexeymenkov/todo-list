<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api\CreateChecklist;


use DemoCode\TodoList\Entry\Api\ApiRequest;
use Symfony\Component\Validator\Constraints as Assert;

class CreateChecklistRequest implements ApiRequest
{
    /**
     * @var string|null
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    private $title;

    public function mergeFromJsonString(string $json): void
    {
        $data = json_decode($json, true);
        $this->title = $data["title"] ?? null;
    }

    public static function createNew(string $title): self
    {
        $obj = new self();
        $obj->title = $title;

        return $obj;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }
}