<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api;


interface ApiRequest
{
    public function mergeFromJsonString(string $json): void;
}