<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api;

use DemoCode\TodoList\Application\Domain;

class Checklist implements \JsonSerializable
{
    /**
     * @var Domain\Checklist
     */
    private $checklist;

    /**
     * Checklist constructor.
     * @param Domain\Checklist $checklist
     */
    public function __construct(Domain\Checklist $checklist)
    {
        $this->checklist = $checklist;
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->checklist->getId()->toString(),
            "title" => $this->checklist->getTitle(),
            "createdAt" => $this->checklist->getCreatedAt()->format(\DateTime::ATOM),
            "updatedAt" => $this->checklist->getCreatedAt()->format(\DateTime::ATOM),
            "todoList" => array_map(function (Domain\Todo $todo) {
                return new Todo($todo);
            }, $this->checklist->getTodoList()),
        ];
    }
}