<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api;


use Assert\Assertion;

class ResponseError implements \JsonSerializable
{
    /**
     * @var string
     */
    private $message;
    /**
     * @var string
     */
    private $code;
    /**
     * @var ConstraintViolation[]
     */
    private $violations;

    /**
     * ResponseError constructor.
     * @param string $message
     * @param string $code
     * @param ConstraintViolation[] $violations
     */
    public function __construct(string $message, string $code, array $violations = [])
    {
        Assertion::allIsInstanceOf($violations, ConstraintViolation::class);
        $this->message = $message;
        $this->code = $code;
        $this->violations = $violations;
    }

    public function jsonSerialize()
    {
        return [
            "message" => $this->message,
            "code" => $this->code,
            "violations" => $this->violations
        ];
    }
}