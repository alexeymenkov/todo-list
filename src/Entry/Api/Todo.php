<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api;


use DemoCode\TodoList\Application\Domain;

class Todo implements \JsonSerializable
{
    /**
     * @var Domain\Todo
     */
    private $todo;

    /**
     * Todo constructor.
     * @param Domain\Todo $todo
     */
    public function __construct(Domain\Todo $todo)
    {
        $this->todo = $todo;
    }

    public function jsonSerialize()
    {
        return [
            "id" => $this->todo->getId()->toString(),
            "title" => $this->todo->getTitle(),
            "position" => $this->todo->getPosition(),
            "isCompleted" => $this->todo->isCompleted(),
            "createdAt" => $this->todo->getCreatedAt()->format(\DateTime::ATOM),
            "updatedAt" => $this->todo->getCreatedAt()->format(\DateTime::ATOM),
        ];
    }
}