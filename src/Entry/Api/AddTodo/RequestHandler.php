<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api\AddTodo;


use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\UseCase\AddTodo;
use DemoCode\TodoList\Entry\Api\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RequestHandler
{
    /**
     * @var AddTodo\Interactor
     */
    private $interactor;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * RequestHandler constructor.
     * @param AddTodo\Interactor $interactor
     * @param ResponseFactory $responseFactory
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(
        AddTodo\Interactor $interactor,
        ResponseFactory $responseFactory,
        UrlGeneratorInterface $urlGenerator
    )
    {
        $this->interactor = $interactor;
        $this->responseFactory = $responseFactory;
        $this->urlGenerator = $urlGenerator;
    }

    public function addTodo(AddTodoRequest $request): Response
    {
        $domainRequest = new AddTodo\AddTodoRequest(
            new ChecklistId($request->getChecklistId()),
            $request->getTitle(),
            $request->isCompleted(),
            $request->getPosition()
        );

        $result = $this->interactor->addTodo($domainRequest);

        if (!$result->isSuccessful()) {
            return $this->responseFactory->createErrorResponse($result->getError());
        }

        $response = $this->responseFactory->createEmptyResponse(Response::HTTP_CREATED);
        $resourceUrl = $this->urlGenerator->generate("api.checklist.remove_todo", [
            "checklistId" => $request->getChecklistId(),
            "todoId" => $result->getTodo()->getId()
        ]);
        $response->headers->set("Location", $resourceUrl);

        return $response;
    }
}