<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api\UpdateChecklist;


use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\UseCase\UpdateChecklist;
use DemoCode\TodoList\Entry\Api\Checklist;
use DemoCode\TodoList\Entry\Api\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;

class RequestHandler
{
    /**
     * @var UpdateChecklist\Interactor
     */
    private $interactor;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * RequestHandler constructor.
     * @param UpdateChecklist\Interactor $interactor
     * @param ResponseFactory $responseFactory
     */
    public function __construct(UpdateChecklist\Interactor $interactor, ResponseFactory $responseFactory)
    {
        $this->interactor = $interactor;
        $this->responseFactory = $responseFactory;
    }

    public function updateChecklist(string $checklistId, UpdateChecklistRequest $request): Response
    {
        $domainRequest = new UpdateChecklist\UpdateChecklistRequest(
            new ChecklistId($checklistId),
            $request->getTitle()
        );

        $result = $this->interactor->updateChecklist($domainRequest);

        if (!$result->isSuccessful()) {
            return $this->responseFactory->createErrorResponse($result->getError());
        }

        return $this->responseFactory->createResponse(new Checklist($result->getChecklist()));
    }
}