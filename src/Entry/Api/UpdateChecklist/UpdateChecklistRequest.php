<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api\UpdateChecklist;


use DemoCode\TodoList\Entry\Api\ApiRequest;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateChecklistRequest implements ApiRequest
{
    /**
     * @var string|null
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $title;

    public function mergeFromJsonString(string $json): void
    {
        $data = json_decode($json, true);
        $this->title = $data["title"] ?? null;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }
}