<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api\DeleteChecklist;


use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\UseCase\DeleteChecklist\Interactor;
use DemoCode\TodoList\Entry\Api\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;

class RequestHandler
{
    /**
     * @var Interactor
     */
    private $interactor;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * RequestHandler constructor.
     * @param Interactor $interactor
     * @param ResponseFactory $responseFactory
     */
    public function __construct(Interactor $interactor, ResponseFactory $responseFactory)
    {
        $this->interactor = $interactor;
        $this->responseFactory = $responseFactory;
    }

    public function deleteChecklist(string $checklistId): Response
    {
        $this->interactor->deleteChecklist(new ChecklistId($checklistId));

        return $this->responseFactory->createEmptyResponse(Response::HTTP_NO_CONTENT);
    }
}