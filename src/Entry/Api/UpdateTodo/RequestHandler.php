<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api\UpdateTodo;


use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Domain\TodoId;
use DemoCode\TodoList\Application\UseCase\UpdateTodo;
use DemoCode\TodoList\Entry\Api\ResponseFactory;
use DemoCode\TodoList\Entry\Api\Todo;
use Symfony\Component\HttpFoundation\Response;

class RequestHandler
{
    /**
     * @var UpdateTodo\Interactor
     */
    private $interactor;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * RequestHandler constructor.
     * @param UpdateTodo\Interactor $interactor
     * @param ResponseFactory $responseFactory
     */
    public function __construct(UpdateTodo\Interactor $interactor, ResponseFactory $responseFactory)
    {
        $this->interactor = $interactor;
        $this->responseFactory = $responseFactory;
    }

    public function updateTodo(string $checklistId, string $todoId, UpdateTodoRequest $request): Response
    {
        $result = $this->interactor->updateTodo(new UpdateTodo\UpdateTodoRequest(
            new ChecklistId($checklistId),
            new TodoId($todoId),
            $request->getTitle(),
            $request->isCompleted(),
            $request->getPosition()
        ));

        if (!$result->isSuccessful()) {
            return $this->responseFactory->createErrorResponse($result->getError());
        }

        return $this->responseFactory->createResponse(new Todo($result->getTodo()));
    }
}