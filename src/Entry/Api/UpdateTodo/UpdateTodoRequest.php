<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api\UpdateTodo;


use DemoCode\TodoList\Entry\Api\ApiRequest;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateTodoRequest implements ApiRequest
{
    /**
     * @var string|null
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $title;
    /**
     * @var bool|null
     * @Assert\Type(type="bool")
     * @Assert\NotNull()
     */
    private $completed;
    /**
     * @var int|null
     * @Assert\Type(type="integer")
     * @Assert\NotNull()
     */
    private $position;

    public static function createNew(string $title, bool $completed, int $position): self
    {
        $obj = new self();
        $obj->title = $title;
        $obj->completed = $completed;
        $obj->position = $position;

        return $obj;
    }

    public function mergeFromJsonString(string $json): void
    {
        $data = json_decode($json, true);
        $this->title = $data["title"] ?? null;
        $this->completed = $data["completed"] ?? null;
        $this->position = $data["position"] ?? null;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return bool|null
     */
    public function isCompleted(): ?bool
    {
        return $this->completed;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }
}