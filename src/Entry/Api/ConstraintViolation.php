<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api;


class ConstraintViolation implements \JsonSerializable
{
    /**
     * @var string
     */
    private $propertyPath;
    /**
     * @var string
     */
    private $message;

    /**
     * ConstraintViolation constructor.
     * @param string $propertyPath
     * @param string $message
     */
    public function __construct(string $propertyPath, string $message)
    {
        $this->propertyPath = $propertyPath;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getPropertyPath(): string
    {
        return $this->propertyPath;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    public function jsonSerialize()
    {
        return [
            "propertyPath" => $this->propertyPath,
            "message" => $this->message,
        ];
    }
}