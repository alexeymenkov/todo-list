<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api\RemoveTodo;


use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Domain\TodoId;
use DemoCode\TodoList\Application\UseCase\RemoveTodo\Interactor;
use DemoCode\TodoList\Entry\Api\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;

class RequestHandler
{
    /**
     * @var Interactor
     */
    private $interactor;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * RequestHandler constructor.
     * @param Interactor $interactor
     * @param ResponseFactory $responseFactory
     */
    public function __construct(Interactor $interactor, ResponseFactory $responseFactory)
    {
        $this->interactor = $interactor;
        $this->responseFactory = $responseFactory;
    }

    public function removeTodo(string $checklistId, string $todoId): Response
    {
        $result = $this->interactor->removeTodo(new ChecklistId($checklistId), new TodoId($todoId));

        if (!$result->isSuccessful()) {
            return $this->responseFactory->createErrorResponse($result->getError());
        }

        return $this->responseFactory->createEmptyResponse(Response::HTTP_NO_CONTENT);
    }
}