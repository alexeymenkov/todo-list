<?php declare(strict_types=1);


namespace DemoCode\TodoList\Entry\Api\FindChecklist;


use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\UseCase\FindChecklist\Interactor;
use DemoCode\TodoList\Entry\Api\Checklist;
use DemoCode\TodoList\Entry\Api\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;

class RequestHandler
{
    /**
     * @var Interactor
     */
    private $interactor;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * RequestHandler constructor.
     * @param Interactor $interactor
     * @param ResponseFactory $responseFactory
     */
    public function __construct(Interactor $interactor, ResponseFactory $responseFactory)
    {
        $this->interactor = $interactor;
        $this->responseFactory = $responseFactory;
    }

    public function findChecklist(string $checklistId): Response
    {
        $findChecklistResult = $this->interactor->findChecklist(new ChecklistId($checklistId));

        if (!$findChecklistResult->isSuccessful()) {
            return $this->responseFactory->createErrorResponse($findChecklistResult->getError());
        }

        $checklist = new Checklist($findChecklistResult->getChecklist());

        return $this->responseFactory->createResponse($checklist);
    }
}