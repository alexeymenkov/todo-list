<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\Domain;


use Assert\Assertion;

class Checklist
{
    /**
     * @var ChecklistId
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var Todo[]
     */
    private $todoList = [];
    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;
    /**
     * @var \DateTimeImmutable
     */
    private $updatedAt;

    /**
     * Checklist constructor.
     * @param ChecklistId $id
     * @param string $title
     * @param Todo[] $todoList
     * @param \DateTimeImmutable $createdAt
     * @param \DateTimeImmutable $updatedAt
     * @throws \Assert\AssertionFailedException
     */
    public function __construct(
        ChecklistId $id,
        string $title,
        array $todoList,
        \DateTimeImmutable $createdAt,
        \DateTimeImmutable $updatedAt
    ) {
        $this->id = $id;
        $this->title = $title;
        foreach ($todoList as $item) {
            Assertion::isInstanceOf($item, Todo::class);
            $this->todoList[$item->getId()->toString()] = $item;
        }

        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    public static function createNew(string $title): self
    {
        return new self(ChecklistId::new(), $title, [], new \DateTimeImmutable(), new \DateTimeImmutable());
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        $this->actualizeUpdateDate();

        return $this;
    }

    /**
     * @return ChecklistId
     */
    public function getId(): ChecklistId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Todo[]
     */
    public function getTodoList(): array
    {
        return array_values($this->todoList);
    }

    public function addTodo(Todo $todo): self
    {
        Assertion::keyNotExists($this->todoList, $todo->getId()->toString());

        $this->todoList[$todo->getId()->toString()] = $todo;
        $this->actualizeUpdateDate();

        return $this;
    }

    public function removeTodo(TodoId $todoId): self
    {
        if (!isset($this->todoList[$todoId->toString()])) {
            return $this;
        }

        unset($this->todoList[$todoId->toString()]);
        $this->actualizeUpdateDate();

        return $this;
    }

    /**
     * @param TodoId $todoId
     * @return bool
     */
    public function hasTodo(TodoId $todoId): bool
    {
        return isset($this->todoList[$todoId->toString()]);
    }

    /**
     * @param TodoId $todoId
     * @return Todo
     * @throws \Assert\AssertionFailedException
     */
    public function getTodo(TodoId $todoId): Todo
    {
        Assertion::keyExists($this->todoList, $todoId->toString());

        return $this->todoList[$todoId->toString()];
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    private function actualizeUpdateDate(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}