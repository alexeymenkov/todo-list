<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\Domain;


class Todo
{
    /**
     * @var TodoId
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var bool
     */
    private $completed;
    /**
     * @var int
     */
    private $position;
    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;
    /**
     * @var \DateTimeImmutable
     */
    private $updatedAt;

    /**
     * @param TodoId $id
     * @param string $title
     * @param bool $completed
     * @param int $position
     * @param \DateTimeImmutable $createdAt
     * @param \DateTimeImmutable $updatedAt
     */
    public function __construct(
        TodoId $id,
        string $title,
        bool $completed,
        int $position,
        \DateTimeImmutable $createdAt,
        \DateTimeImmutable $updatedAt
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->completed = $completed;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->position = $position;
    }

    public static function createNew(string $title, bool $completed, int $position): self
    {
        return new self(
            TodoId::new(),
            $title,
            $completed,
            $position,
            new \DateTimeImmutable(),
            new \DateTimeImmutable()
        );
    }

    /**
     * @return TodoId
     */
    public function getId(): TodoId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->completed;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }
}