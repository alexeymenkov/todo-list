<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\Domain;


use Ramsey\Uuid\Uuid;

class TodoId
{
    /**
     * @var string
     */
    private $uuid;

    /**
     * ChecklistId constructor.
     * @param string $uuid
     */
    public function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    public static function new(): self
    {
        return new self(Uuid::uuid4()->toString());
    }

    public function __toString()
    {
        return $this->uuid;
    }

    public function toString(): string
    {
        return $this->uuid;
    }
}