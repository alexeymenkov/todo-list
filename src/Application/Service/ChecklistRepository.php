<?php


namespace DemoCode\TodoList\Application\Service;


use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Domain\ChecklistId;

interface ChecklistRepository
{
    public function findChecklist(ChecklistId $checklistId): ?Checklist;
}