<?php


namespace DemoCode\TodoList\Application\Service;


use DemoCode\TodoList\Application\Domain\Checklist;

interface ChecklistPersister
{
    public function saveChecklist(Checklist $checklist): void;
}