<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\Service;


use MyCLabs\Enum\Enum;

/**
 * Class ChecklistOperationError
 * @package DemoCode\TodoList\Application\UseCase\FindChecklist
 * @method static ChecklistOperationError ChecklistNotFound()
 * @method static ChecklistOperationError TodoNotFound()
 * @method static ChecklistOperationError None()
 */
class ChecklistOperationError extends Enum
{
    const None = "None";
    const ChecklistNotFound = "ChecklistNotFound";
    const TodoNotFound = "TodoNotFound";

    public function isNone(): bool
    {
        return $this->equals(self::None());
    }
}