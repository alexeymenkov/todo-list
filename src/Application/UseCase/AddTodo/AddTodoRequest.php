<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\AddTodo;


use DemoCode\TodoList\Application\Domain\ChecklistId;

class AddTodoRequest
{
    /**
     * @var ChecklistId
     */
    private $checklistId;
    /**
     * @var string
     */
    private $title;
    /**
     * @var bool
     */
    private $completed;
    /**
     * @var int
     */
    private $position;

    /**
     * AddTodoRequest constructor.
     * @param ChecklistId $checklistId
     * @param string $title
     * @param bool $completed
     * @param int $position
     */
    public function __construct(ChecklistId $checklistId, string $title, bool $completed, int $position)
    {
        $this->checklistId = $checklistId;
        $this->title = $title;
        $this->completed = $completed;
        $this->position = $position;
    }

    /**
     * @return ChecklistId
     */
    public function getChecklistId(): ChecklistId
    {
        return $this->checklistId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->completed;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }
}