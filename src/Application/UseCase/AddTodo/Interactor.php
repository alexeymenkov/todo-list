<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\AddTodo;


use DemoCode\TodoList\Application\Domain\Todo;
use DemoCode\TodoList\Application\Service\ChecklistPersister;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;

class Interactor
{
    /**
     * @var ChecklistRepository
     */
    private $checklistRepository;

    /**
     * @var ChecklistPersister
     */
    private $checklistPersister;

    /**
     * Interactor constructor.
     * @param ChecklistRepository $checklistRepository
     * @param ChecklistPersister $checklistPersister
     */
    public function __construct(ChecklistRepository $checklistRepository, ChecklistPersister $checklistPersister)
    {
        $this->checklistRepository = $checklistRepository;
        $this->checklistPersister = $checklistPersister;
    }

    public function addTodo(AddTodoRequest $addTodoRequest): AddTodoResult
    {
        $checklist = $this->checklistRepository->findChecklist($addTodoRequest->getChecklistId());

        if ($checklist === null) {
            return AddTodoResult::createWithError(ChecklistOperationError::ChecklistNotFound());
        }

        $todo = Todo::createNew(
            $addTodoRequest->getTitle(),
            $addTodoRequest->isCompleted(),
            $addTodoRequest->getPosition()
        );
        $checklist->addTodo($todo);

        $this->checklistPersister->saveChecklist($checklist);

        return AddTodoResult::createNew($todo);
    }
}