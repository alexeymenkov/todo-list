<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\CreateChecklist;


use Assert\Assertion;

class CreateChecklistRequest
{
    /**
     * @var string
     */
    private $title;

    /**
     * AddChecklistRequest constructor.
     * @param string $title
     * @throws \Assert\AssertionFailedException
     */
    public function __construct(string $title)
    {
        Assertion::notBlank($title);
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}