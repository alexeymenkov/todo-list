<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\CreateChecklist;


use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Service\ChecklistPersister;

class Interactor
{
    /**
     * @var ChecklistPersister
     */
    private $checklistPersister;

    /**
     * Interactor constructor.
     * @param ChecklistPersister $checklistPersister
     */
    public function __construct(ChecklistPersister $checklistPersister)
    {
        $this->checklistPersister = $checklistPersister;
    }

    public function createChecklist(CreateChecklistRequest $addChecklistRequest): CreateChecklistResult
    {
        $checklist = Checklist::createNew($addChecklistRequest->getTitle());

        $this->checklistPersister->saveChecklist($checklist);

        return CreateChecklistResult::create($checklist);
    }
}