<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\CreateChecklist;


use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;

class CreateChecklistResult
{
    /**
     * @var ChecklistOperationError
     */
    private $error;
    /**
     * @var Checklist|null
     */
    private $checklist;

    /**
     * AddChecklistResult constructor.
     * @param Checklist $checklist
     * @param ChecklistOperationError $error
     */
    private function __construct(?Checklist $checklist, ChecklistOperationError $error)
    {
        $this->error = $error;
        $this->checklist = $checklist;
    }

    public static function create(Checklist $checklist): self
    {
        return new self($checklist, ChecklistOperationError::None());
    }

    public static function createWithError(ChecklistOperationError $error): self
    {
        return new self(null, $error);
    }

    /**
     * @return Checklist
     */
    public function getChecklist(): Checklist
    {
        if (!$this->isSuccessful()) {
            throw new \LogicException();
        }

        return $this->checklist;
    }

    /**
     * @return ChecklistOperationError
     */
    public function getError(): ChecklistOperationError
    {
        return $this->error;
    }

    /**
     * @return bool
     */
    public function isSuccessful(): bool
    {
        return $this->error->isNone();
    }
}