<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\FindChecklist;


use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;
use DemoCode\TodoList\Application\Service\ChecklistRepository;

class Interactor
{
    /**
     * @var ChecklistRepository
     */
    private $repository;

    /**
     * Interactor constructor.
     * @param ChecklistRepository $repository
     */
    public function __construct(ChecklistRepository $repository)
    {
        $this->repository = $repository;
    }

    public function findChecklist(ChecklistId $todoListId): FindChecklistResult
    {
        $checklist = $this->repository->findChecklist($todoListId);

        if ($checklist === null) {
            return FindChecklistResult::createWithError(ChecklistOperationError::ChecklistNotFound());
        }

        return FindChecklistResult::create($checklist);
    }
}