<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\FindChecklist;


use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;

class FindChecklistResult
{
    /**
     * @var Checklist
     */
    private $checklist;
    /**
     * @var ChecklistOperationError
     */
    private $error;

    private function __construct(?Checklist $checklist, ?ChecklistOperationError $error)
    {
        $this->checklist = $checklist;
        $this->error = $error;
    }

    public static function create(Checklist $checklist): self
    {
        return new self($checklist, ChecklistOperationError::None());
    }

    public static function createWithError(ChecklistOperationError $error): self
    {
        return new self(null, $error);
    }

    /**
     * @return Checklist|null
     */
    public function getChecklist(): ?Checklist
    {
        return $this->checklist;
    }

    /**
     * @return ChecklistOperationError
     */
    public function getError(): ChecklistOperationError
    {
        return $this->error;
    }

    public function isSuccessful(): bool
    {
        return $this->error->isNone();
    }
}