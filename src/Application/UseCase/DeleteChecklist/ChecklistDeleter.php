<?php


namespace DemoCode\TodoList\Application\UseCase\DeleteChecklist;


use DemoCode\TodoList\Application\Domain\ChecklistId;

interface ChecklistDeleter
{
    public function deleteChecklist(ChecklistId $checklistId): void;
}