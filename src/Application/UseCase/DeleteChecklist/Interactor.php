<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\DeleteChecklist;


use DemoCode\TodoList\Application\Domain\ChecklistId;

class Interactor
{
    /**
     * @var ChecklistDeleter
     */
    private $checklistDeleter;

    /**
     * Interactor constructor.
     * @param ChecklistDeleter $checklistDeleter
     */
    public function __construct(ChecklistDeleter $checklistDeleter)
    {
        $this->checklistDeleter = $checklistDeleter;
    }

    public function deleteChecklist(ChecklistId $checklistId): DeleteChecklistResult
    {
        $this->checklistDeleter->deleteChecklist($checklistId);

        return DeleteChecklistResult::create();
    }
}