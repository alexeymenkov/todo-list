<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\UpdateChecklist;


use DemoCode\TodoList\Application\Service\ChecklistPersister;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;

class Interactor
{
    /**
     * @var ChecklistRepository
     */
    private $checklistRepository;

    /**
     * @var ChecklistPersister
     */
    private $checklistPersister;

    /**
     * Interactor constructor.
     * @param ChecklistRepository $checklistRepository
     * @param ChecklistPersister $checklistPersister
     */
    public function __construct(ChecklistRepository $checklistRepository, ChecklistPersister $checklistPersister)
    {
        $this->checklistRepository = $checklistRepository;
        $this->checklistPersister = $checklistPersister;
    }

    public function updateChecklist(UpdateChecklistRequest $request): UpdateChecklistResult
    {
        $checklist = $this->checklistRepository->findChecklist($request->getChecklistId());

        if ($checklist === null) {
            return UpdateChecklistResult::createWithError(ChecklistOperationError::ChecklistNotFound());
        }

        $checklist->setTitle($request->getTitle());
        $this->checklistPersister->saveChecklist($checklist);

        return UpdateChecklistResult::create($checklist);
    }
}