<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\UpdateChecklist;


use Assert\Assertion;
use DemoCode\TodoList\Application\Domain\ChecklistId;

class UpdateChecklistRequest
{
    /**
     * @var ChecklistId
     */
    private $checklistId;
    /**
     * @var string
     */
    private $title;

    /**
     * UpdateChecklistRequest constructor.
     * @param ChecklistId $checklistId
     * @param string $title
     * @throws \Assert\AssertionFailedException
     */
    public function __construct(ChecklistId $checklistId, string $title)
    {
        Assertion::notBlank($title);
        $this->checklistId = $checklistId;
        $this->title = $title;
    }

    /**
     * @return ChecklistId
     */
    public function getChecklistId(): ChecklistId
    {
        return $this->checklistId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}