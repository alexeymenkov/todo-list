<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\UpdateChecklist;


use DemoCode\TodoList\Application\Domain\Checklist;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;

class UpdateChecklistResult
{
    /**
     * @var Checklist
     */
    private $checklist;
    /**
     * @var ChecklistOperationError
     */
    private $error;

    /**
     * UpdateChecklistResult constructor.
     * @param Checklist|null $checklist
     * @param ChecklistOperationError $error
     */
    private function __construct(?Checklist $checklist, ChecklistOperationError $error)
    {
        $this->checklist = $checklist;
        $this->error = $error;
    }

    public static function create(Checklist $checklist): self
    {
        return new self($checklist, ChecklistOperationError::None());
    }

    public static function createWithError(ChecklistOperationError $error): self
    {
        return new self(null, $error);
    }

    /**
     * @return Checklist
     */
    public function getChecklist(): Checklist
    {
        if (!$this->isSuccessful()) {
            throw new \LogicException();
        }

        return $this->checklist;
    }

    /**
     * @return ChecklistOperationError
     */
    public function getError(): ChecklistOperationError
    {
        return $this->error;
    }

    public function isSuccessful(): bool
    {
        return $this->error->isNone();
    }
}