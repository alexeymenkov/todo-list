<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\RemoveTodo;


use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Domain\TodoId;
use DemoCode\TodoList\Application\Service\ChecklistPersister;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;

class Interactor
{
    /**
     * @var ChecklistRepository
     */
    private $checklistRepository;
    /**
     * @var ChecklistPersister
     */
    private $checklistPersister;

    /**
     * Interactor constructor.
     * @param ChecklistRepository $checklistRepository
     * @param ChecklistPersister $checklistPersister
     */
    public function __construct(
        ChecklistRepository $checklistRepository,
        ChecklistPersister $checklistPersister
    ) {
        $this->checklistRepository = $checklistRepository;
        $this->checklistPersister = $checklistPersister;
    }

    public function removeTodo(ChecklistId $checklistId, TodoId $todoId): RemoveTodoResult
    {
        $checklist = $this->checklistRepository->findChecklist($checklistId);

        if ($checklist === null) {
            return new RemoveTodoResult(ChecklistOperationError::ChecklistNotFound());
        }

        if (!$checklist->hasTodo($todoId)) {
            return new RemoveTodoResult(ChecklistOperationError::TodoNotFound());
        }

        $checklist->removeTodo($todoId);

        $this->checklistPersister->saveChecklist($checklist);

        return RemoveTodoResult::create();
    }
}