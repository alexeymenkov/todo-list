<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\RemoveTodo;


use DemoCode\TodoList\Application\Service\ChecklistOperationError;

class RemoveTodoResult
{
    /**
     * @var ChecklistOperationError
     */
    private $error;

    /**
     * RemoveTodoResult constructor.
     * @param ChecklistOperationError $error
     */
    public function __construct(ChecklistOperationError $error)
    {
        $this->error = $error;
    }

    public static function create(): self
    {
        return new self(ChecklistOperationError::None());
    }

    /**
     * @return ChecklistOperationError
     */
    public function getError(): ChecklistOperationError
    {
        return $this->error;
    }

    public function isSuccessful(): bool
    {
        return $this->error->isNone();
    }
}