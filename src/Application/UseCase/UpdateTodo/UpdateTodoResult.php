<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\UpdateTodo;


use DemoCode\TodoList\Application\Domain\Todo;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;

class UpdateTodoResult
{
    /**
     * @var Todo|null
     */
    private $todo;
    /**
     * @var ChecklistOperationError
     */
    private $error;

    /**
     * UpdateTodoResult constructor.
     * @param Todo|null $todo
     * @param ChecklistOperationError $error
     */
    private function __construct(?Todo $todo, ChecklistOperationError $error)
    {
        $this->todo = $todo;
        $this->error = $error;
    }

    public static function createWithError(ChecklistOperationError $error): self
    {
        return new self(null, $error);
    }

    public static function create(Todo $todo): self
    {
        return new self($todo, ChecklistOperationError::None());
    }

    /**
     * @return Todo
     */
    public function getTodo(): Todo
    {
        if (!$this->isSuccessful()) {
            throw new \LogicException();
        }

        return $this->todo;
    }

    /**
     * @return ChecklistOperationError
     */
    public function getError(): ChecklistOperationError
    {
        return $this->error;
    }

    public function isSuccessful(): bool
    {
        return $this->error->isNone();
    }
}