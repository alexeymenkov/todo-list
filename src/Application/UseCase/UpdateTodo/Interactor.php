<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\UpdateTodo;


use DemoCode\TodoList\Application\Domain\Todo;
use DemoCode\TodoList\Application\Service\ChecklistPersister;
use DemoCode\TodoList\Application\Service\ChecklistRepository;
use DemoCode\TodoList\Application\Service\ChecklistOperationError;

class Interactor
{
    /**
     * @var ChecklistRepository
     */
    private $checklistRepository;
    /**
     * @var ChecklistPersister
     */
    private $checklistPersister;

    /**
     * Interactor constructor.
     * @param ChecklistRepository $checklistRepository
     * @param ChecklistPersister $checklistPersister
     */
    public function __construct(ChecklistRepository $checklistRepository, ChecklistPersister $checklistPersister)
    {
        $this->checklistRepository = $checklistRepository;
        $this->checklistPersister = $checklistPersister;
    }

    public function updateTodo(UpdateTodoRequest $request): UpdateTodoResult
    {
        $checklist = $this->checklistRepository->findChecklist($request->getChecklistId());

        if ($checklist === null) {
            return UpdateTodoResult::createWithError(ChecklistOperationError::ChecklistNotFound());
        }

        if (!$checklist->hasTodo($request->getTodoId())) {
            return UpdateTodoResult::createWithError(ChecklistOperationError::TodoNotFound());
        }

        $todo = $checklist->getTodo($request->getTodoId());
        $updatedTodo = new Todo(
            $request->getTodoId(),
            $request->getTitle(),
            $request->isCompleted(),
            $request->getPosition(),
            $todo->getCreatedAt(),
            new \DateTimeImmutable()
        );

        $checklist
            ->removeTodo($request->getTodoId())
            ->addTodo($updatedTodo);

        $this->checklistPersister->saveChecklist($checklist);

        return UpdateTodoResult::create($updatedTodo);
    }
}