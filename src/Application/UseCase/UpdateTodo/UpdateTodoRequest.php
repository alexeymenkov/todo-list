<?php declare(strict_types=1);


namespace DemoCode\TodoList\Application\UseCase\UpdateTodo;


use DemoCode\TodoList\Application\Domain\ChecklistId;
use DemoCode\TodoList\Application\Domain\TodoId;

class UpdateTodoRequest
{
    /**
     * @var ChecklistId
     */
    private $checklistId;
    /**
     * @var TodoId
     */
    private $todoId;
    /**
     * @var string
     */
    private $title;
    /**
     * @var bool
     */
    private $completed;
    /**
     * @var int
     */
    private $position;

    /**
     * UpdateTodoRequest constructor.
     * @param ChecklistId $checklistId
     * @param TodoId $todoId
     * @param string $title
     * @param bool $completed
     * @param int $position
     */
    public function __construct(
        ChecklistId $checklistId,
        TodoId $todoId,
        string $title,
        bool $completed,
        int $position
    ) {
        $this->checklistId = $checklistId;
        $this->todoId = $todoId;
        $this->title = $title;
        $this->completed = $completed;
        $this->position = $position;
    }

    /**
     * @return ChecklistId
     */
    public function getChecklistId(): ChecklistId
    {
        return $this->checklistId;
    }

    /**
     * @return TodoId
     */
    public function getTodoId(): TodoId
    {
        return $this->todoId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->completed;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }
}